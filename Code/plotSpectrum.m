function [spectPlot] = plotSpectrum(data)
    spectPlot = plot(data(:,1),data(:,2));
    title('Optical Spectrum');
    ylabel('Intensity (counts)');
    xlabel('\lambda (nm)');
    grid on
    axis tight
end

% function [spectPlot] = plotSpectrum(data,title,xLabel,yLabel)
%     spectPlot = plot(data(:,1),data(:,2));
%     title(title);
%     ylabel(yLabel);
%     xlabel(xLabel);
%     grid on
%     axis tight
% end