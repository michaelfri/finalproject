function [prc,plots,axis] = imgProc(imgs,roi)
plots = struct;
prc = struct;
m_Hbr = [ 2000 10000 ];
m_HbO2 = [ 200 10000 ];
a = m_Hbr(1)/(m_Hbr(1)-m_HbO2(1));
b = m_Hbr(2)/(m_Hbr(1)-m_HbO2(1));
filter.std = [ 0 0 0 0 1 0 0 0 0; 0 0 1 1 1 1 1 0 0; 0 1 1 1 1 1 1 1 0; 0 1 1 1 1 1 1 1 0; 1 1 1 1 1 1 1 1 1; 0 1 1 1 1 1 1 1 0; 0 1 1 1 1 1 1 1 0; 0 0 1 1 1 1 1 0 0; 0 0 0 0 1 0 0 0 0 ];
filter.avg = fspecial('disk',9);
bl_flow = zeros(roi.("R"+num2str(1)).num,size(imgs.I001R01,2),size(imgs.I001R01,3));
bl_so2 = bl_flow;
bl_hbr = bl_flow;
bl_thc = bl_flow;
for ss = 1:roi.sessions
    for num = 1:roi.("R"+num2str(ss)).num
        %THC
        axis.THC(ss,num,1:2) = mkcxs(imgs.("I"+num2str(ss,'%03d')+"G"+num2str(num,'%02d')));
        %prc.THC.("I"+num2str(ss,'%03d')+"R"+num2str(num,'%02d')) = imgNorm(squeeze(mean(imgs.("I"+num2str(ss,'%03d')+"G"+num2str(num,'%02d')),1)),unifyAxis(axis.THC));
        prc.THC.("I"+num2str(ss,'%03d')+"R"+num2str(num,'%02d')) =  -log10(squeeze(mean(imgs.("I"+num2str(ss,'%03d')+"G"+num2str(num,'%02d')),1)));
        plots.THC(ss,num) = meanD(-log10(imgs.("I"+num2str(ss,'%03d')+"G"+num2str(num,'%02d'))));
        if ss == 1
             bl_thc(num,:,:) = prc.THC.("I"+num2str(ss,'%03d')+"R"+num2str(num,'%02d'));
        elseif ss == 2 || num == 1
            prc.BL.THC = bl_thc;
        end
        %HBR
        axis.HBR(ss,num,1:2) = mkcxs(-log10(imgs.("I"+num2str(ss,'%03d')+"R"+num2str(num,'%02d'))));
        %prc.HBR.("I"+num2str(ss,'%03d')+"R"+num2str(num,'%02d')) = imgNorm(squeeze(mean(imgs.("I"+num2str(ss,'%03d')+"R"+num2str(num,'%02d')),1)),unifyAxis(axis.HBR));
        prc.HBR.("I"+num2str(ss,'%03d')+"R"+num2str(num,'%02d')) = -log10(squeeze(mean(imgs.("I"+num2str(ss,'%03d')+"R"+num2str(num,'%02d')),1)));
        plots.HBR(ss,num) = meanD(-log10(imgs.("I"+num2str(ss,'%03d')+"R"+num2str(num,'%02d'))));
        if ss == 1
             bl_hbr(num,:,:) = prc.HBR.("I"+num2str(ss,'%03d')+"R"+num2str(num,'%02d'));
        elseif ss == 2 || num == 1
            prc.BL.HBR = bl_hbr;
        end
        %FLOW
            flow = (imfilter(prc.HBR.("I"+num2str(ss,'%03d')+"R"+num2str(num,'%02d')),filter.avg,'symmetric')./stdfilt(prc.HBR.("I"+num2str(ss,'%03d')+"R"+num2str(num,'%02d')),filter.std)).^2;
        prc.FLOW.("I"+num2str(ss,'%03d')+"R"+num2str(num,'%02d')) = clearNaN(flow);
        axis.FLOW(ss,num,1:2) = mkcxs(flow);
        plots.FLOW(ss,num) = mean2D(flow,flow);
        
        if ss == 1
             bl_flow(num,:,:) = flow;
        elseif ss == 2 || num == 1
            prc.BL.FLOW = bl_flow;
        end
        %FLOW2
        f = imgs.("I"+num2str(ss,'%03d')+"G"+num2str(num,'%02d'));
        for i = 1:size(f,1)
            f(i,:,:) = imfilter(squeeze(imgs.("I"+num2str(ss,'%03d')+"G"+num2str(num,'%02d'))(i,:,:)),filter.avg,'symmetric')./stdfilt(squeeze(imgs.("I"+num2str(ss,'%03d')+"G"+num2str(num,'%02d'))(i,:,:)),filter.std).^2;
        end
        flow = mean(f,1);
        prc.FLOW2.("I"+num2str(ss,'%03d')+"R"+num2str(num,'%02d')) = squeeze(flow);
        axis.FLOW2(ss,num,1:2) = mkcxs(flow);
        plots.FLOW2(ss,num) = mean(flow,'all');
        clear flow;
        %SO2
        axis.SO2(ss,num,1:2) = mkcxs(a-b*(prc.THC.("I"+num2str(ss,'%03d')+"R"+num2str(num,'%02d'))./prc.HBR.("I"+num2str(ss,'%03d')+"R"+num2str(num,'%02d'))));
        prc.SO2.("I"+num2str(ss,'%03d')+"R"+num2str(num,'%02d')) = a-b*(prc.THC.("I"+num2str(ss,'%03d')+"R"+num2str(num,'%02d'))./prc.HBR.("I"+num2str(ss,'%03d')+"R"+num2str(num,'%02d')));
        plots.SO2(ss,num) = mean(prc.SO2.("I"+num2str(ss,'%03d')+"R"+num2str(num,'%02d')),'all');
        
        if ss == 1
             bl_so2(num,:,:) = prc.SO2.("I"+num2str(ss,'%03d')+"R"+num2str(num,'%02d'));
        elseif ss == 2 || num == 1
            prc.BL.SO2 = bl_so2;
        end
        %Scattering
        prc.SCAT.("I"+num2str(ss,'%03d')+"R"+num2str(num,'%02d')) = (imgNorm(prc.HBR.("I"+num2str(ss,'%03d')+"R"+num2str(num,'%02d')),unifyAxis(axis.HBR))-imgNorm(prc.THC.("I"+num2str(ss,'%03d')+"R"+num2str(num,'%02d')),unifyAxis(axis.THC)))/(650-532);
        axis.SCAT(ss,num,1:2) = mkcxs(clearNaN(prc.SCAT.("I"+num2str(ss,'%03d')+"R"+num2str(num,'%02d'))));
        plots.SCAT(ss,num) = mean(clearNaN(prc.SCAT.("I"+num2str(ss,'%03d')+"R"+num2str(num,'%02d'))),'all');
        %Ratio
%         axis.RATIO(ss,num,1:2) = mkcxs(imgs.("I"+num2str(ss,'%03d')+"R"+num2str(num,'%02d'))./(imgs.("I"+num2str(ss,'%03d')+"G"+num2str(num,'%02d'))+0.0001));
%         %prc.RATIO.("I"+num2str(ss,'%03d')+"R"+num2str(num,'%02d')) = imgNorm(squeeze(mean(imgs.("I"+num2str(ss,'%03d')+"G"+num2str(num,'%02d')),1)),unifyAxis(axis.THC));
%         prc.RATIO.("I"+num2str(ss,'%03d')+"R"+num2str(num,'%02d')) = squeeze(mean(imgs.("I"+num2str(ss,'%03d')+"R"+num2str(num,'%02d'))./(imgs.("I"+num2str(ss,'%03d')+"G"+num2str(num,'%02d'))+0.0001),1));
%         plots.RATIO(ss,num) = mean(imgNorm(mean(imgs.("I"+num2str(ss,'%03d')+"R"+num2str(num,'%02d')),1),unifyAxis(axis.HBR))./imgNorm(mean(imgs.("I"+num2str(ss,'%03d')+"W"+num2str(num,'%02d'))+0.0001,1),unifyAxis(axis.THC)),'all');
%         axis.RATIO(ss,num,1:2) = mkcxs(prc.RATIO.("I"+num2str(ss,'%03d')+"R"+num2str(num,'%02d')));
                
        %prc.RATIO.("I"+num2str(ss,'%03d')+"R"+num2str(num,'%02d')) = imgNorm(squeeze(mean(imgs.("I"+num2str(ss,'%03d')+"G"+num2str(num,'%02d')),1)),unifyAxis(axis.THC));
%         prc.RW.("I"+num2str(ss,'%03d')+"R"+num2str(num,'%02d')) = squeeze(imgNorm(mean(imgs.("I"+num2str(ss,'%03d')+"R"+num2str(num,'%02d')),1),norm)./(imgNorm(mean(imgs.("I"+num2str(ss,'%03d')+"W"+num2str(num,'%02d')),1),norm)+0.0001));
%         img = reshape(prc.RW.("I"+num2str(ss,'%03d')+"R"+num2str(num,'%02d')),[],1);
%         img(img > 100) = [];
%         prc.RW.("I"+num2str(ss,'%03d')+"R"+num2str(num,'%02d'))(prc.RW.("I"+num2str(ss,'%03d')+"R"+num2str(num,'%02d')) > 100) = mean(img);
%         plots.RW(ss,num) = mean(prc.RW.("I"+num2str(ss,'%03d')+"R"+num2str(num,'%02d')),'all');
%         %prc.RATIO.("I"+num2str(ss,'%03d')+"R"+num2str(num,'%02d')) = imgNorm(squeeze(mean(imgs.("I"+num2str(ss,'%03d')+"G"+num2str(num,'%02d')),1)),unifyAxis(axis.THC));
%         prc.RW.("I"+num2str(ss,'%03d')+"R"+num2str(num,'%02d')) = squeeze(mean(imgs.("I"+num2str(ss,'%03d')+"R"+num2str(num,'%02d')),1)./(mean(imgs.("I"+num2str(ss,'%03d')+"W"+num2str(num,'%02d')),1)+0.0001));
%         plots.RW(ss,num,1:2) = mean(mean(imgs.("I"+num2str(ss,'%03d')+"R"+num2str(num,'%02d')),1)./(mean(imgs.("I"+num2str(ss,'%03d')+"W"+num2str(num,'%02d')),1)+0.0001),'all');
        %Scattering
%         prc.SCAT.("I"+num2str(ss,'%03d')+"R"+num2str(num,'%02d')) = mat2gray(imgNorm(prc.HBR.("I"+num2str(ss,'%03d')+"R"+num2str(num,'%02d')),unifyAxis(axis.HBR)) - imgNorm(prc.THC.("I"+num2str(ss,'%03d')+"R"+num2str(num,'%02d')),unifyAxis(axis.THC)));
%         plots.SCAT(ss,num) = mean(prc.SCAT.("I"+num2str(ss,'%03d')+"R"+num2str(num,'%02d')),'all');
%         axis.SCAT(ss,num,1:2) = mkcxs(prc.SCAT.("I"+num2str(ss,'%03d')+"R"+num2str(num,'%02d')));
    end
end

clear bl_flow bl_hbr bl_so2;
bl_flow = meanI(prc.BL.FLOW);
bl_so2 = meanI(prc.BL.SO2);
bl_hbr = meanI(prc.BL.HBR);
bl_thc = meanI(prc.BL.THC);
for ss = 1:roi.sessions
    for num = 1:roi.("R"+num2str(ss)).num
        %CMRO2
        prc.CMR.("I"+num2str(ss,'%03d')+"R"+num2str(num,'%02d')) = (1+prc.FLOW.("I"+num2str(ss,'%03d')+"R"+num2str(num,'%02d'))./bl_flow).*((1-prc.SO2.("I"+num2str(ss,'%03d')+"R"+num2str(num,'%02d')))./(1-bl_so2))-1;
        plots.CMR(ss,num) = mean(prc.CMR.("I"+num2str(ss,'%03d')+"R"+num2str(num,'%02d')),'all');
        axis.CMR(ss,num,1:2) = mkcxs(prc.CMR.("I"+num2str(ss,'%03d')+"R"+num2str(num,'%02d')));
        %HBRR
        prc.HBRR.("I"+num2str(ss,'%03d')+"R"+num2str(num,'%02d')) = prc.HBR.("I"+num2str(ss,'%03d')+"R"+num2str(num,'%02d')) - bl_hbr;
        plots.HBRR(ss,num) = mean2D(bl_hbr,prc.HBR.("I"+num2str(ss,'%03d')+"R"+num2str(num,'%02d')));
        axis.HBRR(ss,num,1:2) = mkcxs(prc.HBRR.("I"+num2str(ss,'%03d')+"R"+num2str(num,'%02d')));
        %THCR
        prc.THCR.("I"+num2str(ss,'%03d')+"R"+num2str(num,'%02d')) = prc.THC.("I"+num2str(ss,'%03d')+"R"+num2str(num,'%02d')) - bl_thc;
        plots.THCR(ss,num) = mean2D(bl_thc,prc.THC.("I"+num2str(ss,'%03d')+"R"+num2str(num,'%02d')));
        axis.THCR(ss,num,1:2) = mkcxs(prc.THCR.("I"+num2str(ss,'%03d')+"R"+num2str(num,'%02d')));
        %SO2R
        axis.SO2R(ss,num,1:2) = mkcxs(a-b*(prc.THCR.("I"+num2str(ss,'%03d')+"R"+num2str(num,'%02d'))./prc.HBRR.("I"+num2str(ss,'%03d')+"R"+num2str(num,'%02d'))));
        prc.SO2R.("I"+num2str(ss,'%03d')+"R"+num2str(num,'%02d')) = a-b*(prc.THCR.("I"+num2str(ss,'%03d')+"R"+num2str(num,'%02d'))./prc.HBRR.("I"+num2str(ss,'%03d')+"R"+num2str(num,'%02d')));
        plots.SO2R(ss,num) = mean(prc.SO2R.("I"+num2str(ss,'%03d')+"R"+num2str(num,'%02d')),'all');
        %FLOWR
            flowR = (imfilter(prc.HBRR.("I"+num2str(ss,'%03d')+"R"+num2str(num,'%02d')),filter.avg,'symmetric')./stdfilt(prc.HBRR.("I"+num2str(ss,'%03d')+"R"+num2str(num,'%02d')),filter.std)).^2;
            ffR = reshape(flowR,[],1);
            ffR(isnan(ffR)) = [];
        prc.FLOWR.("I"+num2str(ss,'%03d')+"R"+num2str(num,'%02d')) = flowR;
        axis.FLOWR(ss,num,1:2) = mkcxs(flowR);
        plots.FLOWR(ss,num) = mean(ffR,'all');
        prc.SCATR.("I"+num2str(ss,'%03d')+"R"+num2str(num,'%02d')) = (imgNorm(prc.HBRR.("I"+num2str(ss,'%03d')+"R"+num2str(num,'%02d')),unifyAxis(axis.HBRR))-imgNorm(prc.THCR.("I"+num2str(ss,'%03d')+"R"+num2str(num,'%02d')),unifyAxis(axis.THCR)))/(650-532);
        axis.SCATR(ss,num,1:2) = mkcxs(clearNaN(prc.SCAT.("I"+num2str(ss,'%03d')+"R"+num2str(num,'%02d'))));
        plots.SCATR(ss,num) = mean(clearNaN(prc.SCATR.("I"+num2str(ss,'%03d')+"R"+num2str(num,'%02d'))),'all');
    end
end

end