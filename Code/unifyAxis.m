function [axis] = unifyAxis(ax)
axis(1) = mean2(ax(:,:,1)) - 0.5*std2(ax(:,:,1));
axis(2) = mean2(ax(:,:,2)) + 0.5*std2(ax(:,:,2));
end