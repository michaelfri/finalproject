function out = mean2D(base,img)

if size(base) ~= size(img)
    error("Dimension mismatch");
else
    M = [reshape(base,1,[]) reshape(img,1,[])];
    M(M == max(max(img)) | M == min(min(img)) | ~isnumeric(M)) = [];
    out = mean(M);
end
end

function mkVid(filename,pI,gP,aX,Is,Ir,pR)
vid = VideoWriter(filename);
vid.FrameRate = 1;
vid.open
axx = unifyAxis(aX);
uC = 1;
scale = 250/(2*Ir.size+1);
for uA = 1:Ir.sessions
    for uB = 1:Ir.(char("R"+num2str(uA))).num
%         base(uC,:,:) = imresize(fliplr(squeeze(Is.(char("I"+num2str(uA,'%03d')+"W"+num2str(uB,'%02d')))(3,:,:))),scale);
%         cmap(uC,:,:) = imresize(pI.(char("I"+num2str(uA,'%03d')+"R"+num2str(uB,'%02d'))),scale);
        base(uC,:,:) = fliplr(squeeze(Is.(char("I"+num2str(uA,'%03d')+"W"+num2str(uB,'%02d')))(3,:,:)));
        cmap(uC,:,:) = pI.(char("I"+num2str(uA,'%03d')+"R"+num2str(uB,'%02d')));
        uC = uC + 1;
    end
end
disp([size(base) size(cmap)]);
gr = floor(49*mat2gray(reshape(gP',1,[])))+1;
raw = reshape(gP',1,[]);
visual = ones(50,round(uC*1.25),3);
gr = gr(floor((1:1:200)*(length(gr)/201)+1));
disp([size(gr) uA uB uC]);
for uD = 1:(uC-1)
    visual(
    visualN = insertText(imresize(visual,[50 250]),[188 -2],char(num2str(raw(uD))),'FontSize',25,'TextColor',[0.4392 0.6784 0.2784],'BoxOpacity',0);
    writeVideo(vid,[visualN; imresize(clrFusion(squeeze(cmap(uD,:,:)),squeeze(base(uD,:,:)),axx,'parula'),scale)]);
end
vid.close;
end