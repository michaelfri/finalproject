close all;
clc;
% Most recent
%dir = 'c:\%USERPROFILE%\Desktop\Results';
%addpath('c:\Project\Michael_Project\Code\');
prms.dir = uigetdir+"\Arm";
mkdir(prms.dir);
prms.Colors = [0.4392 0.6784 0.2784; 0.7529 0 0; 0.4392 0.2784 0.6784; 91 91 91];
prms.NumOfSessions = 4;
prms.SizeOfROI = 132;
prms.timeLine = {1,1,"Baseline";2,3,"Pressure";3,2,"Pressure";4,3,"Release"};

prms.Events = [ones(5,1) sort(randi(30,5,1))];
[D.I,D.R] = imgLoad(prms.NumOfSessions,prms.SizeOfROI,'r','c:\Users\student\Desktop\201119-1.avi');
for uA = 2:D.R.sessions
    prms.Events = [prms.Events;  uA*ones(2,1) sort(randi(D.R.("R"+num2str(uA)).num,2,1))];
end

% Preview and ROI
for uA = 1:D.R.sessions
    imwrite(mat2gray(D.R.("R"+num2str(uA)).preview),prms.dir+"\Preview"+num2str(uA,'%03d')+".png");
    for uB = 1:D.R.("R"+num2str(uA)).num
        roi(uB,:,:) = mean(D.I.W{uA,uB}(3:end,:,:),1);
    end
    imwrite(mat2gray(squeeze(D.I.W{uA,1}(3,:,:))),prms.dir+"\ROI"+num2str(uA,'%03d')+".png");
end

[D.P,D.S] = imgProc(D.I,D.R);
D.L = genPlot(D.P,D.R,D.I);
%C3 Scattering
        D.A.RSC = mkcxs(D.P.RSC);
fig.graphs.rsc = graphView(D.R,D.L.RSC,prms,"Rayleigh Scattering, Raw Values");
[fig.RSC, ~ ] = frameView(D.P.RSC,D.A.RSC,prms,"Scattering");
saveas(fig.graphs.rsc,prms.dir+"\C3-RSC_Graph.png");
for uC = 1:length(fig.RSC)
    saveas(fig.RSC{uC},prms.dir+"\C3-RSC_"+num2str(uC,'%02d')+".png");
end
%A1 FLOW by HBR
        D.A.FLOW_R = mkcxs(D.P.FLOW_R);
fig.graphs.flow_R = graphView(D.R,D.L.FLOW_R,prms,"Flow by HBR - Calibrated between 0~1");
[fig.flow_R, ~ ] = frameView(D.P.FLOW_R,D.A.FLOW_R,prms,"Flow");
saveas(fig.graphs.flow_R,prms.dir+"\A1-Flow[R]_Graph.png");
for uC = 1:length(fig.flow_R)
    saveas(fig.flow_R{uC},prms.dir+"\A1-Flow[R]_"+num2str(uC,'%02d')+".png");
end
%A2 FLOW by HBR (Raw Values)
        D.A.FLOW_rR = mkcxs(D.P.FLOW_rR);
fig.graphs.flow_rR = graphView(D.R,D.L.FLOW_rR,prms,"Flow by HBR (Raw values) Normalized between 0~1");
[fig.flow_rR, ~ ] = frameView(D.P.FLOW_rR,D.A.FLOW_rR,prms,"Flow");
saveas(fig.graphs.flow_rR,prms.dir+"\A2-Flow[rR]_Graph.png");
for uC = 1:length(fig.flow_rR)
    saveas(fig.flow_rR{uC},prms.dir+"\A2-Flow[rR]_"+num2str(uC,'%02d')+".png");
end
% %A3 FLOW by THC
        D.A.FLOW_G = mkcxs(D.P.FLOW_G);
fig.graphs.flow_G = graphView(D.R,D.L.FLOW_G,prms,"Flow by THC - Calibrated between 0~1");
[fig.flow_G, ~ ] = frameView(D.P.FLOW_G,D.A.FLOW_G,prms,"Flow");
saveas(fig.graphs.flow_G,prms.dir+"\A3-Flow[G]_Graph.png");
for uC = 1:length(fig.flow_G)
    saveas(fig.flow_G{uC},prms.dir+"\A3-Flow[G]_"+num2str(uC,'%02d')+".png");
end
% %A4 FLOW by THC (Raw Values)
        D.A.FLOW_rG = mkcxs(D.P.FLOW_rG);
fig.graphs.flow_rG = graphView(D.R,D.L.FLOW_rG,prms,"Flow by THC (Raw values) - Calibrated between 0~1");
[fig.flow_rG, ~ ] = frameView(D.P.FLOW_rG,D.A.FLOW_rG,prms,"Flow");
saveas(fig.graphs.flow_rG,prms.dir+"\A4-Flow[rG]_Graph.png");
for uC = 1:length(fig.flow_rG)
    saveas(fig.flow_rG{uC},prms.dir+"\A4-Flow[rG]_"+num2str(uC,'%02d')+".png");
end
%A5 FLOW by HBR (Experimental)
        D.A.FLOW_RE = mkcxs(D.P.FLOW_RE);
fig.graphs.flow_RE = graphView(D.R,D.L.FLOW_RE,prms,"Flow - Calibrated between 0~1");
[fig.flow_RE, ~ ] = frameView(D.P.FLOW_RE,D.A.FLOW_RE,prms,"Flow");
saveas(fig.graphs.flow_RE,prms.dir+"\A5-Flow[RE]_Graph.png");
for uC = 1:length(fig.flow_RE)
    saveas(fig.flow_RE{uC},prms.dir+"\A5-Flow[RE]_"+num2str(uC,'%02d')+".png");
 end
% %A6 FLOW by THC (Experimental)
        D.A.FLOW_GE = mkcxs(D.P.FLOW_GE);
fig.graphs.flow_GE = graphView(D.R,D.L.FLOW_GE,prms,"Flow by THC - Calibrated between 0~1");
[fig.flow_GE, ~ ] = frameView(D.P.FLOW_GE,D.A.FLOW_GE,prms,"Flow G[E]");
saveas(fig.graphs.flow_GE,prms.dir+"\A6-Flow[GE]_Graph.png");
for uC = 1:length(fig.flow_GE)
    saveas(fig.flow_GE{uC},prms.dir+"\A6-Flow[GE]_"+num2str(uC,'%02d')+".png");
end
%B1 SO2 by Absorbance
        D.A.SO2_A = mkcxs(D.P.SO2_A);
fig.graphs.so2_A = graphView(D.R,D.L.SO2_A,prms,"SO2 by Absorbance");%, Normalized to match 65% at baseline");
[fig.so2_A, ~ ] = frameView(D.P.SO2_A,D.A.SO2_A,prms,"SO2");
saveas(fig.graphs.so2_A,prms.dir+"\B1-SO2[A]_Graph.png");
for uC = 1:length(fig.so2_A)
    saveas(fig.so2_A{uC},prms.dir+"\B1-SO2[A]_"+num2str(uC,'%02d')+".png");
end
%B2 SO2 by Reflection
        D.A.SO2_R = mkcxs(D.P.SO2_R);
fig.graphs.so2_
R = graphView(D.R,D.L.SO2_R,prms,"SO2 by  Reflection");%, Normalized to match 65% at baseline","Estimated Oxygen Saturation [%]"]);
[fig.so2_R, ~ ] = frameView(D.P.SO2_R,D.A.SO2_R,prms,"SO2");
saveas(fig.graphs.so2_R,prms.dir+"\B2-SO2[R]_Graph.png");
for uC = 1:length(fig.so2_R)
    saveas(fig.so2_R{uC},prms.dir+"\B2-SO2[R]_"+num2str(uC,'%02d')+".png");
end
%C1 Scattering (Linear Approximation)
        D.A.SCAT_L = mkcxs(D.P.SCAT_L);
fig.graphs.scat_L = graphView(D.R,D.L.SCAT_L,prms,"Scattering - Linear, Normalized between 0~1");
[fig.scat_L, ~ ] = frameView(D.P.SCAT_L,D.A.SCAT_L,prms,"Scattering");
saveas(fig.graphs.scat_L,prms.dir+"\C1-SCAT[L]_Graph.png");
for uC = 1:length(fig.scat_L)
    saveas(fig.scat_L{uC},prms.dir+"\C1-SCAT[L]_"+num2str(uC,'%02d')+".png");
end
% %C2 Scattering (Rayleigh)
        D.A.SCAT_R = mkcxs(D.P.SCAT_R);
fig.graphs.scat_R = graphView(D.R,D.L.SCAT_R,prms,"Rayleigh Scattering, Normalized between 0~1");
[fig.scat_R, ~ ] = frameView(D.P.SCAT_R,D.A.SCAT_R,prms,"Scattering");
saveas(fig.graphs.scat_R,prms.dir+"\C2-SCAT[R]_Graph.png");
for uC = 1:length(fig.scat_R)
    saveas(fig.scat_R{uC},prms.dir+"\C2-SCAT[R]_"+num2str(uC,'%02d')+".png");
end
% %C2 Scattering (Rayleigh)
        D.A.SCAT_R = mkcxs(D.P.SCAT_R);
fig.graphs.scat_R = graphView(D.R,D.L.SCAT_R,prms,"Rayleigh Scattering, Normalized between 0~1");
[fig.scat_R, ~ ] = frameView(D.P.SCAT_R,D.A.SCAT_R,prms,"Scattering");
saveas(fig.graphs.scat_R,prms.dir+"\C2-SCAT[R]_Graph.png");
for uC = 1:length(fig.scat_R)
    saveas(fig.scat_R{uC},prms.dir+"\C2-SCAT[R]_"+num2str(uC,'%02d')+".png");
end
%C4 Scattering (Based on blue laser)
        D.A.BSC = mkcxs(D.P.BSC);
fig.graphs.bsc = graphView(D.R,D.L.BSC,prms,"Scattering using blue laser");
[fig.bsc, ~ ] = frameView(D.P.BSC,D.A.BSC,prms,"Scattering");
saveas(fig.graphs.bsc,prms.dir+"\C4-BSC_Graph.png");
for uC = 1:length(fig.bsc)
    saveas(fig.bsc{uC},prms.dir+"\C4-BSC_"+num2str(uC,'%02d')+".png");
end
%D2 CMRO2 (Based on SO2 By Absorbance, regular values flow)
        D.A.CMR_A = mkcxs(D.P.CMR_A);
fig.graphs.cmr_A = graphView(D.R,D.L.CMR_A,prms,"tMRO2 - Using SO2 By Absorbance");
[fig.cmr_A, ~ ] = frameView(D.P.CMR_A,D.A.CMR_A,prms,"tMRO2");
saveas(fig.graphs.cmr_A,prms.dir+"\D2-CMR[A]_Graph.png");
for uC = 1:length(fig.cmr_R)
    saveas(fig.cmr_A{uC},prms.dir+"\D2-CMR[A]_"+num2str(uC,'%02d')+".png");
end
%E1 THC
        D.A.THC = mkcxs(D.P.THC);
fig.graphs.thc = graphView(D.R,D.L.THC,prms,"THC - Scaled between 0~1");
[fig.thc, ~ ] = frameView(D.P.THC,D.A.THC,prms,"THC");
saveas(fig.graphs.thc,prms.dir+"\E1-THC_Graph.png");
for uC = 1:length(fig.thc)
    saveas(fig.thc{uC},prms.dir+"\E1-THC_"+num2str(uC,'%02d')+".png");
end
%E2 THC (Raw Values)
        D.A.THCr = mkcxs(D.P.THCr);
fig.graphs.thc_R = graphView(D.R,D.L.THCr,prms,"THC - Scaled to range 0~1");
[fig.thc_R, ~ ] = frameView(D.P.THCr,D.A.THCr,prms,"THC");
saveas(fig.graphs.thc_R,prms.dir+"\E2-THC[R]_Graph.png");
for uC = 1:length(fig.thc_R)
    saveas(fig.thc_R{uC},prms.dir+"\E2-THC[R]_"+num2str(uC,'%02d')+".png");
end
%F1 HBR
        D.A.HBR = mkcxs(D.P.HBR);
fig.graphs.hbr = graphView(D.R,D.L.HBR,prms,["HBR - Scaled to match 35% at baseline","Estimated HBR [%]"]);
[fig.hbr, ~ ] = frameView(D.P.HBR,D.A.HBR,prms,"HBR");
saveas(fig.graphs.hbr,prms.dir+"\F1-HBR_Graph.png");
for uC = 1:length(fig.hbr)
    saveas(fig.hbr{uC},prms.dir+"\F1-HBR_"+num2str(uC,'%02d')+".png");
end
%F2 HBR (Raw Values)
        D.A.HBRr = mkcxs(D.P.HBRr);
fig.graphs.hbr_R = graphView(D.R,D.L.HBRr,prms,["HBR - Scaled to match 35% at baseline","Estimated HBR [%]"]);
[fig.hbr_R, ~ ] = frameView(D.P.HBRr,D.A.HBRr,prms,"Scattering");
saveas(fig.graphs.hbr_R,prms.dir+"\F2-HBR[R]_Graph.png");
for uC = 1:length(fig.hbr_R)
    saveas(fig.hbr_R{uC},prms.dir+"\F2-HBR[R]_"+num2str(uC,'%02d')+".png");
end

for uA = 1:length(prms.Events)
    iD(uA,:) = prms.Events(uA,:);
    base(uA,:,:) = mat2gray(squeeze(D.I.W{iD(uA,1),iD(uA,2)}(3,:,:)));
    clrF(uA,:,:) = D.P.FLOW_RE{iD(uA,1),iD(uA,2)};
    clrS(uA,:,:) = D.P.SO2_A{iD(uA,1),iD(uA,2)};
    clrC(uA,:,:) = D.P.THCr{iD(uA,1),iD(uA,2)};
    imwrite(squeeze(base(uA,:,:)),prms.dir+"\K1-BW_"+num2str(uA,'%02d')+".png");
    flow(uA,:,:,:) = clrFusion(1,squeeze(clrF(uA,:,:)),D.A.FLOW_RE,'parula');
    thc_(uA,:,:,:) = clrFusion(1,squeeze(clrC(uA,:,:)),D.A.FLOW_GE,'parula');
    so2(uA,:,:,:) = clrFusion(1,squeeze(clrS(uA,:,:)),D.A.SO2_A,'parula');
    flowF(uA,:,:,:) = clrFusion(squeeze(base(uA,:,:)),squeeze(clrF(uA,:,:)),D.A.FLOW_RE,'parula');
    thcF(uA,:,:,:) = clrFusion(squeeze(base(uA,:,:)),squeeze(clrC(uA,:,:)),D.A.THCr,'parula');
    so2F(uA,:,:,:) = clrFusion(squeeze(base(uA,:,:)),squeeze(clrS(uA,:,:)),D.A.SO2_A,'parula');
    imwrite(squeeze(flow(uA,:,:,:)),prms.dir+"\K2-FL_"+num2str(uA,'%02d')+".png");
    imwrite(squeeze(so2(uA,:,:,:)),prms.dir+"\K3-SO_"+num2str(uA,'%02d')+".png");
    imwrite(squeeze(flowF(uA,:,:,:)),prms.dir+"\K4-FL_"+num2str(uA,'%02d')+".png");
    imwrite(squeeze(so2F(uA,:,:,:)),prms.dir+"\K5-SO_"+num2str(uA,'%02d')+".png");
    imwrite(squeeze(thc_(uA,:,:,:)),prms.dir+"\K6-TH_"+num2str(uA,'%02d')+".png");
    imwrite(squeeze(thcF(uA,:,:,:)),prms.dir+"\K7-TH_"+num2str(uA,'%02d')+".png");
end
