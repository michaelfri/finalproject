function [ax] = mkcxs(in)
counts = 144;
iG = 5;
fct = @(vA,vT) (1.0001-vA/vT)^(-1);
hist = reshape(cell2mat(in),1,[]);
hist(~isfinite(hist)) = [];
[cT,xR] = histcounts(hist,counts);
scan = cT(iG:(end-iG));
xX = xR(iG:(end-iG-1));
T = sum(scan);
uC = 0;
    for uA = 1:length(scan)
        for uB = 1:(length(scan)-uA)
            uC = uC + 1;
            S(uC,:) = [uA uB fct(sum(scan(uB:(uA+uB))),T) sum(scan(uB:(uA+uB)))^2/uA];
        end
    end
    R = S;
    R(find(R(:,3) < 20),:) = [];
    v1 = R(find(R(:,3) == max(R(:,3))),:);
    v2 = R(find(R(:,4) == max(R(:,4))),:);
    figure('Name','Hist');plot(xX,scan);
    hold on

    pts = ginput(2);
    [val,p1] = min(abs(xX-pts(1,1)));
    [val,p2] = min(abs(xX-pts(2,1)));
    plot(xX(p1),scan(p1),'b*');
    plot(xX(p2),scan(p2),'b*');
    hold off
    pause(3);
    close('Name','Hist');
    ax = sort([xX(p1) xX(p2)]);
end