function [IMGS,ROI] = imgLoad(sessions,s1ze,mode,filename)
deadPixel = [ 253 364 ];
IMGS = struct;
ROI = struct;
ROI.size = s1ze;
ROI.sessions = sessions;
uX = 0;
N = 0;
TX{1} = {0};
idx(1,:) = [0,0,0,0];
frmCtr = 0;

vid = VideoWriter(filename);
vid.FrameRate = 12;
vid.open

for uA = 1:ROI.sessions
    [ROI.(char("R"+num2str(uA))).name,ROI.(char("R"+num2str(uA))).path] = uigetfile('*.mat');
    ROI.(char("R"+num2str(uA))).num = 0;
    name = strsplit(ROI.(char("R"+num2str(uA))).name,'-');
    ROI.(char("R"+num2str(uA))).name = cell2mat(name(1));
    addpath(ROI.(char("R"+num2str(uA))).path);
    
    while exist(ROI.(char("R"+num2str(uA))).name + "-" + int2str(N + 1) + ".mat",'file') == 2
        N = N + 1;
    end
    ROI.(char("R"+num2str(uA))).num = N;
    if mode == 'r'
        load(string(ROI.(char("R"+num2str(uA))).path) + name(1) + "-" + name(2),'white');
        ROI.(char("R"+num2str(uA))).preview = imPatch(white,deadPixel,'a');
        clear white;
        %ROI.(char("R"+num2str(num)).preview = squeeze(ROI.(char("R"+num2str(num)).preview(1+max(mod(randi(87),s1ze(ROI.(char("R"+num2str(num)).preview,1)-2),2),:,:));
        ROI.(char("R"+num2str(uA))).preview = squeeze(ROI.(char("R"+num2str(uA))).preview(3,:,:));
        figure('Name','Preview');imagesc(ROI.(char("R"+num2str(uA))).preview);title("White Light Illumination - Select ROI");colormap gray
        ctr = round(fliplr(ginput(1)));
        ROI.(char("R"+num2str(uA))).roi = round([(ctr(1)-ROI.size) (ctr(2)-ROI.size)]);
        ROI.(char("R"+num2str(uA))).preview = markROI(ROI.(char("R"+num2str(uA))).preview,[ctr s1ze],10);
        close('Preview');
        figure('Name',"ROI"+num2str(uA));imagesc(ROI.(char("R"+num2str(uA))).preview);title("Selected ROI");colormap gray
        hold on;
        %rectangle('Position',[ROI.(char("R"+num2str(num))).roi(1),ROI.(char("R"+num2str(num))).roi(2),ROI.size*2,ROI.size*2],'LineWidth',2,'LineStyle','--')
        %hold off;
        for uB = 1:N
            load(string(ROI.(char("R"+num2str(uA))).path) + ROI.(char("R"+num2str(uA))).name + "-" + int2str(uB) + ".mat",'white','blue','red','green');
            frame = imPatch(white,deadPixel,'a');
            for uC = 1:size(frame,1)
                frmCtr = frmCtr + 1;
                mov(frmCtr,:,:) = frame(uC,:,:);
            end
            if uB == 1
                X{1} = imPatch(red,deadPixel,[ROI.(char("R"+num2str(uA))).roi(1) ROI.(char("R"+num2str(uA))).roi(2) ROI.size]);
                X{2} = imPatch(green,deadPixel,[ROI.(char("R"+num2str(uA))).roi(1) ROI.(char("R"+num2str(uA))).roi(2) ROI.size]);
                X{3} = imPatch(blue,deadPixel,[ROI.(char("R"+num2str(uA))).roi(1) ROI.(char("R"+num2str(uA))).roi(2) ROI.size]);
                X{4} = imPatch(white,deadPixel,[ROI.(char("R"+num2str(uA))).roi(1) ROI.(char("R"+num2str(uA))).roi(2) ROI.size]);
            else
                X{1} = [X{1}; imPatch(red,deadPixel,[ROI.(char("R"+num2str(uA))).roi(1) ROI.(char("R"+num2str(uA))).roi(2) ROI.size])];
                X{2} = [X{2}; imPatch(green,deadPixel,[ROI.(char("R"+num2str(uA))).roi(1) ROI.(char("R"+num2str(uA))).roi(2) ROI.size])];
                X{3} = [X{3}; imPatch(blue,deadPixel,[ROI.(char("R"+num2str(uA))).roi(1) ROI.(char("R"+num2str(uA))).roi(2) ROI.size])];
                X{4} = [X{3}; imPatch(white,deadPixel,[ROI.(char("R"+num2str(uA))).roi(1) ROI.(char("R"+num2str(uA))).roi(2) ROI.size])];
            end
            uX = uX + 1;
            idx(uX,:) = [uA uB (7*(uX-1)+1) 7*uX];
            clear green red green blue white;
        end
    elseif mode == 'a'
        for uB = 1:N
            load(string(ROI.(char("R"+num2str(uA))).path) + ROI.(char("R"+num2str(uA))).name + "-" + int2str(uB) + ".mat",'white','blue','red','green');
            if uB == 1
                X{1} = imPatch(red,deadPixel,'a');
                X{2} = imPatch(green,deadPixel,'a');
                X{3} = imPatch(blue,deadPixel,'a');
                X{4} = imPatch(white,deadPixel,'a');
            else
                X{1} = [X{1}; imPatch(red,deadPixel,'a')];
                X{2} = [X{2}; imPatch(green,deadPixel,'a')];
                X{3} = [X{2}; imPatch(blue,deadPixel,'a')];
                X{4} = [X{3}; imPatch(white,deadPixel,'a')];
            end
            uX = uX + 1;
            idx(uX,:) = [uA uB (7*(uX-1)+1) 7*uX];
            clear red green blue white;
        end
    end
    if uA == 1
        TX{1} = X{1};
        TX{2} = X{2};
        TX{3} = X{3};
        TX{4} = X{4};
        
    else
        TX{1} = [TX{1}; X{1}];
        TX{2} = [TX{2}; X{2}];
        TX{3} = [TX{3}; X{3}];
        TX{4} = [TX{4}; X{4}];
    end
end
TX{1} = mat2gray(TX{1});
TX{2} = mat2gray(TX{2});
TX{3} = mat2gray(TX{3});
TX{4} = mat2gray(TX{4});
for uD = 1:uX
    IMGS.R{idx(uD,1),idx(uD,2)} = TX{1}(idx(uD,3):idx(uD,4),:,:);
    IMGS.G{idx(uD,1),idx(uD,2)} = TX{2}(idx(uD,3):idx(uD,4),:,:);
    IMGS.B{idx(uD,1),idx(uD,2)} = TX{3}(idx(uD,3):idx(uD,4),:,:);
    IMGS.W{idx(uD,1),idx(uD,2)} = TX{4}(idx(uD,3):idx(uD,4),:,:);
end
for uH = 1:frmCtr
    writeVideo(vid,mat2gray(squeeze(mov(uH,:,:))));
end
close(vid);
end