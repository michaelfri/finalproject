function [pL] = genPlot(pI,iR,iI)
for uI = 1:iR.sessions
    for uJ = 1:iR.(char("R"+num2str(uI))).num
        pL.THC(uI,uJ) = mean(pI.THC{uI,uJ},'all');
        pL.RSC(uI,uJ) = mean(pI.RSC{uI,uJ},'all');
        pL.BSC(uI,uJ) = mean(pI.BSC{uI,uJ},'all');
        pL.THCr(uI,uJ) = mean(pI.THCr{uI,uJ},'all');
        pL.HBR(uI,uJ) = mean(pI.HBR{uI,uJ},'all');
        pL.HBRr(uI,uJ) = mean(pI.HBRr{uI,uJ},'all');
        pL.FLOW_R(uI,uJ) = mean(pI.FLOW_R{uI,uJ},'all');
        pL.FLOW_rR(uI,uJ) = mean(pI.FLOW_rR{uI,uJ},'all');
        pL.FLOW_G(uI,uJ) = mean(pI.FLOW_G{uI,uJ},'all');
        pL.FLOW_rG(uI,uJ) = mean(pI.FLOW_rG{uI,uJ},'all');
        pL.FLOW_RE(uI,uJ) = mean(pI.FLOW_RE{uI,uJ},'all');
        pL.FLOW_GE(uI,uJ) = mean(pI.FLOW_GE{uI,uJ},'all');
        pL.SO2_A(uI,uJ) = mean(pI.SO2_A{uI,uJ},'all');
        pL.SO2_R(uI,uJ) = mean(pI.SO2_R{uI,uJ},'all');
        pL.SO2_A(uI,uJ) = mean(pI.SO2_A{uI,uJ},'all');
        pL.SO2_R(uI,uJ) = mean(pI.SO2_R{uI,uJ},'all');
        pL.SCAT_L(uI,uJ) = mean(pI.SCAT_L{uI,uJ},'all');
        pL.SCAT_rL(uI,uJ) = mean(pI.SCAT_rL{uI,uJ},'all');
        pL.SCAT_R(uI,uJ) = mean(pI.SCAT_R{uI,uJ},'all');
        pL.CMR_A(uI,uJ) = mean(pI.CMR_A{uI,uJ},'all');
        pL.CMR_R(uI,uJ) = mean(pI.CMR_R{uI,uJ},'all');
    end
end

end
