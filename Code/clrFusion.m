function [out]=clrFusion(grey,clr,caxis,cmap)
%STRSPLIT  Split string at delimiter
%   OUT = clrFusion(grey,clr,caxis,cmap) Takes a base monochrome image
%   given as 'grey' (mXn matrix) and generates a colorized image according
%   to the values of another image given as 'clr' (mXn matrix) between
%   given 'caxis' values. caxis is a 2x1 vector. The parameter 'cmap' is a  char
%   containing the name of the colormap to be used for the colorization.
%   
%   The Function outputs a mxnx3 image that can be viewed with the command
%   "image" or exported using "imwrite".

%   Written by Michael Friedman. For inquaries: michaelfri@gmail.com

if length(caxis) ~= 2
    caxis = [min(min(clr)) max(max(clr))];
end
    map = parula(513);
%     for i = 1:size(clr,1)
%         for j = 1:size(clr,2)
%             out(i,j,:) = map(clr(i,j)+1,:);
%         end
%     end
    switch cmap
        case 'hsv'
            map = hsv(513);
        case 'summer'
            map = summer(513);
        case 'winter'
            map = winter(513);
        case 'spring'
            map = spring(513);
        case 'autumn'
            map = autumn(513);
        case 'jet'
            map = jet(513);
        case 'hot'
            map = hot(513);
        case 'cool'
            map = cool(513);
        case 'gray'
            map = gray(513);
        case 'bone'
            map = bone(513);
        case 'copper'
            map = copper(513);
        case 'pink'
            map = pink(513);
        case 'lines'
            map = lines(513);
        case 'colorcube'
            map = colorcube(513);
        case 'prism'
            map = prism(513);
        case 'flag'
            map = flag(513);
        case 'white'
            map = white(513);
        otherwise
            map = parula(513);
    end

%map = jet(513);
shade = zeros(513,513,3);
delta2 = 0.0025;
shade(:,:,1) = (ones(513,1)*((-318*delta2):delta2:(194*delta2)) + map(:,1)*ones(1,513))';
shade(:,:,2) = (ones(513,1)*((-318*delta2):delta2:(194*delta2)) + map(:,2)*ones(1,513))';
shade(:,:,3) = (ones(513,1)*((-318*delta2):delta2:(194*delta2)) + map(:,3)*ones(1,513))';
shade(shade > 1) = 1;
shade(shade < 0) = 0;

% clr = clr/(caxis(2)-caxis(1));
clr(clr > caxis(2)) = caxis(2);
clr(clr < caxis(1)) = caxis(1);
clr = clr - min(min(clr));
clr = clr/max(max(clr));
clr = round(512*clr)+1;
% clr = round(512*clr);
out = zeros(size(clr,1),size(clr,2),3);
if grey == 1
    for k = 1:3
    for i = 1:size(clr,1)
        for j = 1:size(clr,2)
            out(i,j,k) = shade(319,clr(i,j),k);
        end
    end    
    end
else
grey = grey - min(min(grey));
grey = grey/max(max(grey));
grey = round(512*grey)+1;
%clr = clr + delta;
% clr(clr >= 512) = 512;
% clr(clr <= 0) = 1;

for k = 1:3
    for i = 1:size(clr,1)
        for j = 1:size(clr,2)
            out(i,j,k) = shade(grey(i,j),clr(i,j),k);
        end
    end    
end
end
end

% function [out,map] = normA(gray,clr)
% if size(gray) ~= size(clr)
%     error('Mismatched dimentions. Please use matrices with the same size.');
% else
%     dims = size(gray);
% gry = mat2gray(gray);
% gry = round(512*gry)+1;
% clr = round(512*clr)+1;
% cmap = parula(513);
% out = zeros(dims(1),dims(2),3);
% %delta =1./2*(1-max(cmap));
% delta = [0.0025 0.0025 0.0025];
% map = zeros(513,513,3);
% map(:,:,1) = (ones(513,1))*((-256*delta(1)):delta(1):(256*delta(1)))+cmap(:,1)*ones(1,513);
% map(:,:,2) = (ones(513,1))*((-256*delta(1)):delta(1):(256*delta(1)))+cmap(:,2)*ones(1,513);
% map(:,:,3) = (ones(513,1))*((-256*delta(1)):delta(1):(256*delta(1)))+cmap(:,3)*ones(1,513);
% clr(clr >= 513) = 513;
% clr(clr <= 0) = 1;
% gry(gry >= 513) = 513;
% gry(gry <= 0) = 1;
% for i = 1:dims(1)
%     for j = 1:dims(2)
%         out(i,j,1:3) = map(gry(i,j),clr(i,j),:);
%     end
% end
% %UNTITLED Summary of this function goes here
% %   Detailed explanation goes here
% 
% end
