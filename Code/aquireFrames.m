function [okay] = aquireFrames(s,plan,name,varargin)
% About
warning('off','imaq:gige:adaptorPropertyHealed');
global config;
config.session = 1;
dur = [datetime datetime datetime];
timeout = 1;
frame = [492 656];
skipMSK = logical(plan);
skipMsg = [ "red channel" , "green channel" , "blue channel" , "white channel" , "ambient light" ];
if (exist((name+"-1.mat"), 'file'))
    if sum(ismember(varargin,'f')) > 0
        delete([name '-*']);
        warning('Overwriting experiment with the same name');
    else
    error('Experiment with that name already exists in this directory. Please select a different name or input ''f'' to overwrite');
    end
end
exposure = [ 7000 40000 40000 50000 120000 ];
gainVect = [ 0 15 15 15 0];
writeDigitalPin(s.ard,config.pulseEnable,true);
triggerconfig(s.cam, 'hardware','DeviceSpecific','DeviceSpecific');
s.TriggerSelector = 'FrameStart';
s.img.TriggerActivation = 'RisingEdge';
s.img.TriggerSource = 'InputLines';
s.img.TriggerMode = 'On';

s.img.ExposureAuto = 'Off';
s.img.ExposureTime = exposure(4);
s.img.GainAuto = 'Off';
s.img.Gain = gainVect(4);

cnt = 5*ones(1,10);
preview(s.cam);
start(s.cam);
writeDigitalPin(s.ard,config.ledPin,true);
writePosition(s.srv,config.srvPos(4));
disp('Position sample. Push button when ready.');
while sum(cnt) > 44
    pause(0.2);
    cnt = [readVoltage(s.ard,config.pushBtn) cnt(1:end-1)];
end
writeDigitalPin(s.ard,config.ledPin,true);
clear cnt;
stop(s.cam);
% s.img.GainAuto = 'Off';
% s.cam.FramesPerTrigger = 3;
% for i = 1:5
%     if skipMSK(i)
%         cprintf('text',"Adjusting "+skipMsg(i)+'. [');
%         for j = 1:size(config.gainVect,2)
%             start(s.cam);
%             s.img.Gain = config.gainVect(j);
%             writeDigitalPin(s.ard,config.ledPin,i == 4);
%             writePosition(s.srv,config.srvPos(i));
%             while s.cam.framesAvailable < 2
%                 for q=0:(1/(timeout*(60/0.15))):1
%                     if q<0.98
%                         pause(0.05);
%                         if s.cam.framesAvailable >=2
%                             break
%                         end
%                     else
%                         error('Timeout reached');
%                     end
%                 end
%             end
%             hist = histcounts(squeeze(max(permute(getdata(s.cam,2),[3 4 1 2]))),[1 2:(253/config.gain(2)):254 255]);
%             rank = sum(hist(3:(size(hist,2)-2))); %*log10(min(max(10,hist(2:end-1))));
%             if gainVect(2,i) < rank
%                 gainVect(2,i) = rank;
%                 gainVect(1,i) = j;
%             end
%             stop(s.cam);
%             cprintf('text','#');
%         end
%         cprintf('text',']\n');
%     end
% end
% disp('Camera calibration complete');
% s.cam.FramesPerTrigger = max(max(plan),1);
% preview(s.cam);
% for i=1:5
%     if skipMSK(i)
%         s.img.GainAuto = 'Continuous';
%         start(s.cam);
%         writePosition(s.srv,config.srvPos(i));
%         writeDigitalPin(s.ard,config.ledPin,i == 4);
%         cnt = 5*ones(1,10);
%         gain = [s.img.Gain*(0.2:0.1:2) 0];
%         gain(end) = size(gain,2)-1;
%         s.img.GainAuto = 'Off';
%         disp('Calibrating '+skipMsg(i));
%         while sum(cnt) > 44
%             pause(0.2);
%             cnt = [readVoltage(s.ard,config.pushBtn) cnt(1:end-1)];
%             s.img.Gain = min(23,gain(mod(gain(end),size(gain,2)-1)+1));
%             pause(0.2);
%             cnt = [readVoltage(s.ard,config.pushBtn) cnt(1:end-1)];
%             gain(end) = gain(end)+1;
%             gainVect(1,i) = min(23,gain(mod(gain(end)-1,size(gain,2)-1)+1));
%             pause(0.2);
%         end
%         stop(s.cam);
%         disp('done.');
%     end
% end
% disp(gainVect);
% gainVect(1,:) = [ 2 23 0 7 9 ];
% disp('Manual Calibration done. now starting...');
% cprintf('cyan', ("Experiment "+'"'+name+'"'+", Session #"+num2str(config.session)+'\n'));


% Data pre-allocation
if skipMSK(1)
    red = zeros(plan(6),plan(1),frame(1),frame(2));
end
if skipMSK(2)
    green = zeros(plan(6),plan(2),frame(1),frame(2));
end
if skipMSK(3)
    blue = zeros(plan(6),plan(3),frame(1),frame(2));
end
if skipMSK(4)
    white = zeros(plan(6),plan(4),frame(1),frame(2));
end
if skipMSK(5)
    dark = zeros(plan(6),plan(5),frame(1),frame(2));
end

for r = 1:plan(6)
    for i = 1:5
        if skipMSK(i)
            s.img.Gain = gainVect(i);
            s.img.ExposureTime = exposure(i);
            start(s.cam);
            writeDigitalPin(s.ard,config.ledPin,i == 4);
            writePosition(s.srv,config.srvPos(i));
            pause(1);
            while plan(i) > s.cam.framesAvailable 
                for q=0:(1/(timeout*(60/0.15))):1
                    if q<0.98
                        pause(0.15);
                        if plan(i) == s.cam.framesAvailable
                            break
                        end
                    else
                        error('Timeout reached');
                    end
                end
            end
            stop(s.cam);
            switch i
                case 1
                      red(r,1:plan(1),:,:,:) = permute(getdata(s.cam,plan(1)),[3,4,1,2]);
                      cprintf('red',"Red channel done for repetition #"+num2str(r)+'.'); cprintf('text'," took "+int2str(plan(1))+" frames.\n");
                case 2
                      green(r,1:plan(2),:,:,:) = permute(getdata(s.cam,plan(2)),[3,4,1,2]);
                      cprintf('green',"Green channel done for repetition #"+num2str(r)+'.'); cprintf('text'," took "+int2str(plan(2))+" frames.\n");
                case 3
                      blue(r,1:plan(3),:,:,:) = permute(getdata(s.cam,plan(3)),[3,4,1,2]);
                      cprintf('blue',"Blue channel done for repetition #"+num2str(r)+'.'); cprintf('text'," took "+int2str(plan(3))+" frames.\n");
                case 4
                      white(r,1:plan(4),:,:,:) = permute(getdata(s.cam,plan(4)),[3,4,1,2]);
                      cprintf('cyan',"White channel done for repetition #"+num2str(r)+'.'); cprintf('text'," took "+int2str(plan(4))+" frames.\n");
                case 5
                      dark(r,1:plan(5),:,:,:) = permute(getdata(s.cam,plan(5)),[3,4,1,2]);
                      cprintf('cyan',"Ambient light done for repetition #"+num2str(r)+'.'); cprintf('text'," took "+int2str(plan(5))+" frames.\n");
                otherwise
                        error('Unknown error')
            end
        else
            cprintf('text',"Skipping "+skipMsg(i)+".\n");
        end
    end
    writeDigitalPin(s.ard,config.ledPin,false);
    dur(2) = dur(3);
    dur(3) = datetime;
    disp('Time duration for this repetition: '+string(dur(3)-dur(2))+'. Time elapsed: '+string(dur(3)-dur(1))+'.');
    
end

for r=1:plan(6)
    if skipMSK(1)
        data.red(:,:,:) = squeeze(red(r,:,:,:,:));
    end
    if skipMSK(2)
        data.green(:,:,:) = squeeze(green(r,:,:,:,:));
    end
    if skipMSK(3)
        data.blue(:,:,:) = squeeze(blue(r,:,:,:,:));
    end
    if skipMSK(4)
        data.white(:,:,:) = squeeze(white(r,:,:,:,:));
    end
    if skipMSK(5)
        data.dark(:,:,:) = squeeze(dark(r,:,:,:,:));
    end
    save(string(name)+'-'+num2str(r),'-struct','data');
end
    writeDigitalPin(s.ard,config.pulseEnable,false);
    okay = true;
end