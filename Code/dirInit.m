function [expDir] = dirInit(path)
% Directory Initialization

names = struct2cell(dir(strcat('Exp-',string(datetime('today','Format','yyMMdd')),'-*')))';
names=char(names(:,1));
if isempty(names)
dirName = strcat('Exp-',string(datetime('today','Format','yyMMdd')),'-01');
else
dirName = strcat('Exp-',string(datetime('today','Format','yyMMdd')),'-',num2str(max(str2num(names(:,12:13)))+1,'%02.f'));
end
expDir = path+dirName;
mkdir(expDir);
end