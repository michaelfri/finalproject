function [pI,sT] = imgProc(iI,iR)
pI = struct;
m_Hbr = [ 2000 10000 ];
m_HbO2 = [ 200 10000 ];
a = (200/(200-2000));
b = 10000/(200-2000);
filter.std = [ 0 0 0 0 1 0 0 0 0; 0 0 1 1 1 1 1 0 0; 0 1 1 1 1 1 1 1 0; 0 1 1 1 1 1 1 1 0; 1 1 1 1 1 1 1 1 1; 0 1 1 1 1 1 1 1 0; 0 1 1 1 1 1 1 1 0; 0 0 1 1 1 1 1 0 0; 0 0 0 0 1 0 0 0 0 ];
filter.avg = fspecial('disk',9);
calib.hbr = [32.5 12.5];
calib.flow = [1 0];
calib.so2 = [1 0];
calib.cmr = [1 0];
calib.scat = [1 0];
calib.thc = [1 0];
size = 2*iR.size+1;

wL = ([630 532 435]*10^(-9)).^(1);
C = (wL - mean(wL))/((wL(1) - mean(wL))^2+(wL(2) - mean(wL))^2+(wL(3) - mean(wL))^2);
C = [C sum(C)];

for uA = 1:iR.sessions
    for uB = 1:iR.(char("R"+num2str(uA))).num
        %THC
        pI.THCr{uA,uB} = squeeze(nanmean(iI.G{uA,uB},1));
        pI.THC{uA,uB} = -log10(squeeze(nanmean(iI.G{uA,uB},1)));
        sT.THCr(uA,uB,:) = [mean2(pI.THCr{uA,uB}) std2(pI.THCr{uA,uB}) min(min(pI.THCr{uA,uB})) max(max(pI.THCr{uA,uB}))];
        sT.THC(uA,uB,:) = [mean2(pI.THC{uA,uB}) std2(pI.THC{uA,uB}) min(min(pI.THC{uA,uB})) max(max(pI.THC{uA,uB}))];
        %HBR
        pI.HBRr{uA,uB} = squeeze(nanmean(iI.R{uA,uB},1));
        pI.HBR{uA,uB} = -log10(pI.HBRr{uA,uB});
        sT.HBRr(uA,uB,:) = [mean2(pI.HBRr{uA,uB}) std2(pI.HBRr{uA,uB}) min(min(pI.HBRr{uA,uB})) max(max(pI.HBRr{uA,uB}))];
        sT.HBR(uA,uB,:) = [mean2(pI.HBR{uA,uB}) std2(pI.HBR{uA,uB}) min(min(pI.HBR{uA,uB})) max(max(pI.HBR{uA,uB}))];
        if uA == 1
            bl.thcA(uB,:,:) = pI.THC{uA,uB};
            bl.thcrA(uB,:,:) = pI.THCr{uA,uB};
            bl.hbrA(uB,:,:) = pI.HBR{uA,uB};
            bl.hbrrA(uB,:,:) = pI.HBRr{uA,uB};
        elseif uA == 2 && uB == 1
            bl.thc = squeeze(mean(bl.thcA,1));
            bl.thcr = squeeze(mean(bl.thcrA,1));
            bl.hbr = squeeze(mean(bl.hbrA,1));
            bl.hbrr = squeeze(mean(bl.hbrrA,1));
        end
        I = squeeze(iI.R{uA,uB}(4,:,:) + iI.G{uA,uB}(4,:,:) + iI.B{uA,uB}(4,:,:))/3;
        pI.RSC{uA,uB} = C(1)*squeeze(iI.R{uA,uB}(4,:,:)) + C(2)*squeeze(iI.G{uA,uB}(4,:,:)) + C(3)*squeeze(iI.B{uA,uB}(4,:,:)) - C(4)*I;
        sT.RSC(uA,uB,:) = [mean2(pI.RSC{uA,uB}) std2(pI.RSC{uA,uB}) min(min(pI.RSC{uA,uB})) max(max(pI.RSC{uA,uB}))];
        
        pI.BSCr{uA,uB} = squeeze(nanmean(iI.B{uA,uB},1));
        pI.BSC{uA,uB} = -log10(squeeze(nanmean(iI.B{uA,uB},1)));
        sT.BSCr(uA,uB,:) = [mean2(pI.BSCr{uA,uB}) std2(pI.BSCr{uA,uB}) min(min(pI.BSCr{uA,uB})) max(max(pI.BSCr{uA,uB}))];
        sT.BSC(uA,uB,:) = [mean2(pI.BSC{uA,uB}) std2(pI.BSC{uA,uB}) min(min(pI.BSC{uA,uB})) max(max(pI.BSC{uA,uB}))];
    end
end
        pI.THC = imgNorm(pI.THC,calib.thc);
        pI.HBR = imgNorm(pI.HBR,calib.hbr);
        
for uC = 1:iR.sessions
    for uD = 1:iR.(char("R"+num2str(uC))).num

        
        %FLOW - By HBR
        pI.FLOW_R{uC,uD} = clearNaN((imfilter(pI.HBR{uC,uD},filter.avg,'symmetric')./stdfilt(pI.HBR{uC,uD},filter.std)).^2);
        sT.FLOW_R(uC,uD,:) = [mean2(pI.FLOW_R{uC,uD}) std2(pI.FLOW_R{uC,uD}) min(min(pI.FLOW_R{uC,uD})) max(max(pI.FLOW_R{uC,uD}))];
        %FLOW - By HBR (Raw)
        pI.FLOW_rR{uC,uD} = clearNaN((imfilter(pI.HBRr{uC,uD},filter.avg,'symmetric')./stdfilt(pI.HBRr{uC,uD},filter.std)).^2);
        sT.FLOW_rR(uC,uD,:) = [mean2(pI.FLOW_rR{uC,uD}) std2(pI.FLOW_rR{uC,uD}) min(min(pI.FLOW_rR{uC,uD})) max(max(pI.FLOW_rR{uC,uD}))];
        %FLOW - By THC
        pI.FLOW_G{uC,uD} = clearNaN((imfilter(pI.THC{uC,uD},filter.avg,'symmetric')./stdfilt(pI.THC{uC,uD},filter.std)).^2);
        sT.FLOW_G(uC,uD,:) = [mean2(pI.FLOW_G{uC,uD}) std2(pI.FLOW_G{uC,uD}) min(min(pI.FLOW_G{uC,uD})) max(max(pI.FLOW_G{uC,uD}))];
        %FLOW - By THC (Raw)
        pI.FLOW_rG{uC,uD} = clearNaN((imfilter(pI.THCr{uC,uD},filter.avg,'symmetric')./stdfilt(pI.THCr{uC,uD},filter.std)).^2);
        sT.FLOW_rG(uC,uD,:) = [mean2(pI.FLOW_rG{uC,uD}) std2(pI.FLOW_rG{uC,uD}) min(min(pI.FLOW_rG{uC,uD})) max(max(pI.FLOW_rG{uC,uD}))];
        %FLOW - (Experimental) By HBR
        for uX = 2:6
            FLOW(uX,:,:) = clearNaN((imfilter(squeeze(iI.R{uC,uD}(uX,:,:)),filter.avg,'symmetric')./stdfilt(squeeze(iI.R{uC,uD}(uX,:,:)),filter.std)).^2);
        end
        pI.FLOW_RE{uC,uD} = meanN(FLOW,'Image');
        sT.FLOW_RE(uC,uD,:) = [mean2(pI.FLOW_RE{uC,uD}) std2(pI.FLOW_RE{uC,uD})];
        %FLOW - (Experimental) By THC
        for uX = 2:6
            FLOW(uX,:,:) = clearNaN((imfilter(squeeze(iI.G{uC,uD}(uX,:,:)),filter.avg,'symmetric')./stdfilt(squeeze(iI.G{uC,uD}(uX,:,:)),filter.std)).^2);
        end
        pI.FLOW_GE{uC,uD} = meanN(FLOW,'Image');
        sT.FLOW_GE(uC,uD,:) = [mean2(pI.FLOW_GE{uC,uD}) std2(pI.FLOW_GE{uC,uD})];
        if uC == 1
            bl.flow_RA(uD,:,:) = pI.FLOW_R{uC,uD};
            bl.flow_rRA(uD,:,:) = pI.FLOW_rR{uC,uD};
            bl.flow_GA(uD,:,:) = pI.FLOW_G{uC,uD};
            bl.flow_rGA(uD,:,:) = pI.FLOW_rG{uC,uD};
        elseif uC == 2 && uD == 1
            bl.flow_R = squeeze(mean(bl.flow_RA,1));
            bl.flow_rR = squeeze(mean(bl.flow_rRA,1));
            bl.flow_G = squeeze(mean(bl.flow_GA,1));
            bl.flow_rG = squeeze(mean(bl.flow_rGA,1));
        end
        %SO2 - By Absorbance (Raw)
        pI.SO2_A{uC,uD} = clearNaN(-a+b*(pI.THCr{uC,uD}./pI.HBRr{uC,uD}));
        sT.SO2_A(uC,uD,:) = [mean2(pI.SO2_A{uC,uD}) std2(pI.SO2_A{uC,uD})];
        %SO2 - By Reflection 
        pI.SO2_R{uC,uD} = clearNaN(-a+b*(pI.THC{uC,uD}./pI.HBR{uC,uD}));
        sT.SO2_R(uC,uD,:) = [mean2(pI.SO2_R{uC,uD}) std2(pI.SO2_R{uC,uD})];
        if uC == 1
            bl.so2_AA(uD,:,:) = pI.SO2_A{uC,uD};
            bl.so2_RA(uD,:,:) = pI.SO2_R{uC,uD};
        elseif uC == 2 && uD == 1
            bl.so2_A = squeeze(mean(bl.so2_AA,1));
            bl.so2_R = squeeze(mean(bl.so2_RA,1));
        end
    end
end

calib.so2_A = [calib.so2 nanmean(sT.SO2_A(:,:,1)) calib.so2(2)/mean(nanstd(sT.SO2_A(:,:,1)))];
calib.so2_R = [calib.so2 nanmean(sT.SO2_R(:,:,1)) calib.so2(2)/mean(nanstd(sT.SO2_R(:,:,1)))];
% pI.FLOW_R = tfira(pI.FLOW_R,[iR.sessions iR.R1.num]);
% pI.FLOW_G = tfira(pI.FLOW_G,[iR.sessions iR.R1.num]);
% pI.FLOW_rR = tfira(pI.FLOW_rR,[iR.sessions iR.R1.num]);
% pI.FLOW_rG = tfira(pI.FLOW_rG,[iR.sessions iR.R1.num]);
% pI.SO2_A = tfira(pI.SO2_A,[roi.sessions roi.R1.num]);
% pI.SO2_R = tfira(pI.SO2_R,[roi.sessions roi.R1.num]);

        pI.FLOW_R = imgNorm(pI.FLOW_R,calib.flow);
        pI.FLOW_rR = imgNorm(pI.FLOW_rR,calib.flow);
        pI.FLOW_G = imgNorm(pI.FLOW_G,calib.flow);
        pI.FLOW_RE = imgNorm(pI.FLOW_RE,calib.flow);
        pI.FLOW_GE = imgNorm(pI.FLOW_GE,calib.flow);
        pI.SO2_A = imgNorm(pI.SO2_A,calib.so2);
        pI.SO2_R = imgNorm(pI.SO2_R,calib.so2);
        for uE = 1:iR.sessions
    for uF = 1:iR.(char("R"+num2str(uE))).num
        %Scattering - Linear
        pI.SCAT_L{uE,uF} = (pI.HBR{uE,uF}-pI.THC{uE,uF})/(650-532);
        sT.SCAT_L(uE,uF,:) = [mean2(pI.SCAT_L{uE,uF}) std2(pI.SCAT_L{uE,uF})];% min(min(pI.SCAT_L{uA,uB})) max(max(pI.SCAT_L{uA,uB}))];
        %Scattering - Linear (Based on raw data)
        pI.SCAT_rL{uE,uF} = (pI.HBRr{uE,uF}-pI.THCr{uE,uF})/(650-532);
        sT.SCAT_rL(uE,uF,:) = [mean2(pI.SCAT_rL{uE,uF}) std2(pI.SCAT_rL{uE,uF})];
        %Scattering - Rayleigh
        pI.SCAT_R{uE,uF} = (pI.HBR{uE,uF}-pI.THC{uE,uF})/(650-532);
        sT.SCAT_R(uE,uF,:) = [mean2(pI.SCAT_R{uE,uF}) std2(pI.SCAT_R{uE,uF})];
        %Scattering - Rayleigh (Based on raw data)
        pI.SCAT_rR{uE,uF} = (pI.HBRr{uE,uF}-pI.THCr{uE,uF})/(650-532);
        sT.SCAT_rR(uE,uF,:) = [mean2(pI.SCAT_rR{uE,uF}) std2(pI.SCAT_rR{uE,uF})];
        %CMRO2 - Based on Raw Flow and Absorbtion SO2
        pI.CMR_A{uE,uF} = (1+pI.FLOW_rR{uE,uF}./bl.flow_rR).*((1-pI.SO2_A{uE,uF})./(1-bl.so2_A));
        sT.CMR_A(uE,uF,:) = [mean2(pI.CMR_A{uE,uF}) std2(pI.CMR_A{uE,uF})];% min(min(pI.CMR_A{uA,uB})) max(max(pI.CMR_A{uA,uB}))];
        %CMRO2 - Based on Calibrated Flow and Reflection SO2
        pI.CMR_R{uE,uF} = (1+pI.FLOW_rR{uE,uF}./bl.flow_rR).*((1-pI.SO2_R{uE,uF})./(1-bl.so2_R));
        sT.CMR_R(uE,uF,:) = [mean2(pI.CMR_R{uE,uF}) std2(pI.CMR_R{uE,uF})];% min(min(pI.CMR_R{uA,uB})) max(max(pI.CMR_R{uA,uB}))];
    end
        end
        pI.SCAT_L = imgNorm(pI.SCAT_L,calib.scat);
        pI.SCAT_R = imgNorm(pI.SCAT_R,calib.scat);
        pI.CMR_A = imgNorm(pI.CMR_A,calib.cmr);
        pI.CMR_R = imgNorm(pI.CMR_R,calib.cmr);


%         aX.THC = mkcxs(pI.THC);
%         aX.THCr = mkcxs(pI.THCr);
%         aX.HBR = mkcxs(pI.HBR);
%         aX.HBRr = mkcxs(pI.HBRr);
%         aX.FLOW_R = mkcxs(pI.FLOW_R);
%         aX.FLOW_rR = mkcxs(pI.FLOW_rR);
%         aX.FLOW_G = mkcxs(pI.FLOW_G);
%         aX.FLOW_rG = mkcxs(pI.FLOW_rG);
%         aX.FLOW_RE = mkcxs(pI.FLOW_RE);
%         aX.FLOW_GE = mkcxs(pI.FLOW_GE);
%         aX.SO2_A = mkcxs(pI.SO2_A);
%         aX.SO2_R = mkcxs(pI.SO2_R);
%         aX.SO2_A = mkcxs(pI.SO2_A);
%         aX.SO2_R = mkcxs(pI.SO2_R);
%         aX.SCAT_L = mkcxs(pI.SCAT_L);
%         aX.SCAT_rL = mkcxs(pI.SCAT_rL);
%         aX.SCAT_R = mkcxs(pI.SCAT_R);
%         aX.CMR_A = mkcxs(pI.CMR_A);
%         aX.CMR_R = mkcxs(pI.CMR_R);
end
