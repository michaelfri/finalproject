function out = meanN(in,varargin)
if nargin == 2
    if strcmp(varargin{1},'all')
        temp = in(:);
        temp(~isfinite(temp)) = [];
        out = nanmean(temp,'all');
    elseif strcmp(varargin{1},'Image')
        out = zeros(size(in,2),size(in,3));
        for i = 1:size(in,1)
            z(i,:,:) = clearNaN(squeeze(in(i,:,:)));
        end
            for uA = 1:size(z,2)
                for uB = 1:size(z,3)
                    temp = reshape(z(:,uA,uB),1,[]);
                    temp(~isfinite(temp)) = [];
                    out(uA,uB) = nanmean(temp);
                end
            end
    elseif varargin{1} == 1
        out = zeros(1,size(in,2));
        for uA = 1:size(in,2)
            temp = in(:,uA);
            temp(~isfinite(temp)) = [];
            out(uA) = nanmean(temp(:));
        end
    elseif varargin{1} == 2
        out = zeros(1,size(in,1));
        for uA = 1:size(in,1)
            temp = in(:,uA);
            temp(~isfinite(temp)) = [];
            out(uA) = nanmean(temp(:));
        end
    end
elseif nargin == 1
    out = zeros(1,size(in,2));
    for uA = 1:size(in,2)
        temp = in(:,uA);
        temp(isnan(temp)) = [];
        out(uA) = nanmean(temp(:));
    end
end
