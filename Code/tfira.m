function [data] = tfira(in,pars)
for uA = 1:pars(1)
    for uB = 1:pars(2)
        in{uA,uB} = in{uA,uB} - min(min(in{uA,uB}));
    end
end
for uA = 2:pars(1)
   for uB = 1:10
       M1(uB,:,:) = in{uA-1,pars(2)-uB+1};
       M2(uB,:,:) = in{uA,uB};
   end
   F(1,:) = [0 1 0];
   F(uA,:) = [0.5*meanN(M1,'all') std2(permute(M1,[3,1,2]))/std2(permute(M2,[3,1,2])) meanN(M2,'all')];
end
for uA = 1:pars(1)
    for uB = 1:pars(2)
        data{uA,uB} = in{uA,uB} - F(uA,3);
        data{uA,uB} = data{uA,uB}*F(uA,2);
        data{uA,uB} = data{uA,uB}+F(uA,1);
    end
end
end
