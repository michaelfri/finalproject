% Experiment Initiation
addpath('c:\Project\Michael_Project\','c:\Project\Michael_Project\Code\','c:\Users\Student\Desktop\');
config.session = 0;
global config;
config = configure();
sys = hwInit(config,1);
config.experimentPath = dirInit('c:\Users\Student\Desktop\');
cd(config.experimentPath);
warning('off','imaq:gige:adaptorErrorPropSet');
% Test Run

 while i~=0
for i=1:5
writePosition(sys.srv,config.srvPos(i));
writeDigitalPin(sys.ard,config.ledPin,i==4);
pause(2);
end
writeDigitalPin(sys.ard,config.ledPin,false);
end

% CleanUp

disconnect(sys.spect);
delete (sys.spect);

% Delete current experiment
currentDir = pwd;
delete *;
cd ..;
rmdir currentDir;
clear currentDir;
