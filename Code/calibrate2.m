function [out,params] = calibrate2(data1,data2,values)
data1 = data1 - min(data1);
data2 = data2 - min(data2);
data1 = data1 - min(data1);
data2 = data2 - min(data2);
delta1 = std(data1)+ 0.5*(max(data1) - min(data1));
delta2 = std(data2)+ 0.5*(max(data2) - min(data2));
means = [mean(data1(1:30),'all') mean(data2(1:30),'all') mean(data1((end-29):end),'all') mean(data2((end-29):end),'all')];
if means(1) >= means(2)
    data2 = data2 + (means(1) -means(2));
else
    data1 = data1 + (means(2) -means(1));
end
if means(3) >= means(4)
    delta = mean([(max(data)-min(data)),means(1)+std(data(1:30))-means(2)+std(data((end-29):end))]);
else
    delta = mean([(max(data)-min(data)),means(2)+std(data((end-29):end))-means(1)+std(data(1:30))]);
end
params(1) = (abs(values(1) - values(2)))/delta;
data = data * params(1);
params(2) = min(values);
out = data + min(values);
disp(params);
end
%mean(delta(1:pts)),
%(1.05*mean(data((end-pts):end)),