function [s] = hwInit(conf,level)

% Hardware Initialization
s.ard=arduino(conf.comPort,conf.boardType,'libraries','Servo');
s.srv=servo(s.ard,conf.srvPin);
writeDigitalPin(s.ard,conf.pulseEnable,false);

if level >= 1
    if level == 2
        s.cam = videoinput('gentl', 1, 'BayerRG8');
    else
        s.cam = videoinput('gentl', 1, 'Mono8');
    end
    triggerconfig(s.cam, 'immediate');
    s.img = getselectedsource(s.cam);
    %sys.img.PacketSize=8999;

end

if level > 3
    s.spect = icdevice('OceanOptics_OmniDriver.mdd');
    writeDigitalPin(s.ard,conf.pulseEnable,false);
    connect(s.spect);
    disp(s.spect);
        % Set integration time.
    invoke(s.spect, 'setIntegrationTime', conf.spectConf.spectrometerIndex, conf.spectConf.channelIndex, conf.spectConf.integrationTime);
        % Enable correct for detector non-linearity.
    invoke(s.spect, 'setCorrectForDetectorNonlinearity', conf.spectConf.spectrometerIndex, conf.spectConf.channelIndex, conf.spectConf.enable);
        % Enable correct for electrical dark.
    invoke(s.spect, 'setCorrectForElectricalDark', conf.spectConf.spectrometerIndex, conf.spectConf.channelIndex, conf.spectConf.enable);
end
end