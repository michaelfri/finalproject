function [scmap] = scmap(img)

map=im2double(img)/255;
window=strel('disk',4,0);
mean_filter=fspecial('disk',4);
std=stdfilt(map,window.Neighborhood);
avg=imfilter(map,mean_filter,'symmetric');
scmap = std./avg;