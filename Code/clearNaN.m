function out=clearNaN(in)
dims = size(in);
out = zeros(dims(1),dims(2));
in = [in(5:-1:2,:); in; in((end-1):(-1):(end-4),:)];
in = [in(:,5:-1:2) in in(:,(end-1):(-1):(end-4))];
for i = 4+(1:dims(1))
    for j = 4+(1:dims(2))
        if isfinite(in(i,j))
            out(i-4,j-4) = in(i,j);
        else
            patch = reshape(in((i-2):(i+2),(j-3):(j+3)),1,[]);
            patch = [patch in(i-3,(j-2):(j+2))];
            patch = [patch in(i+3,(j-2):(j+2))];
            patch = [patch in(i,j-4)];
            patch = [patch in(i,j+4)];
            patch = [patch in(i-4,j)];
            patch = [patch in(i+4,j)];
            patch(~isfinite(patch)) = [];
            out(i-4,j-4) = mean(patch);
        end
    end
end