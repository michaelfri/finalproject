function [out,params] = calibrate(data,values)
params = [0 min(data)];
means = [mean(data(1:30) - params(2),'all') mean(data((end-29):end) - params(2),'all')];
if means(1) >= means(2)
    delta = means(1)+std(data(1:30))-means(2)+std(data((end-29):end));
else
    delta = means(2)+std(data((end-29):end))-means(1)+std(data(1:30));
end
params(1) = (abs(values(1) - values(2)))/delta;
data = (data-params(2)) * params(1);
params(2) = params(2) + min(values);
out = data + min(values);
disp(params);
end
%mean(delta(1:pts)),
%(1.05*mean(data((end-pts):end)),