function [out] = imgNorm(in,c)
f = @(n,N) (n*ones(1,N/n));
mm = @(x) mean2(x);
ALL = cell2mat(in);
t = cellfun(mm,in);
mM = [min(t(:)) max(t(:))];
ALL = ALL - mM(1);
dims = [size(in{1,1}); size(ALL)];
if ndims(ALL) == 2
    tmp = mat2cell(ALL,f(dims(1,1),dims(2,1)),f(dims(1,2),dims(2,2)));
elseif ndims(ALL) == 3
    tmp = mat2cell(ALL,f(dims(1,1),dims(2,1)),f(dims(1,2),dims(2,2)),f(dims(1,3),dims(2,3)));
end

ALL2 = cell2mat(tmp);
t = cellfun(mm,tmp);
mM = [min(t(:)) max(t(:))];
A = c(1)*ALL2/mM(2);
A = A - mM(1)/mM(2)+c(2);
if ndims(ALL2) == 2
    out = mat2cell(A,f(dims(1,1),dims(2,1)),f(dims(1,2),dims(2,2)));
elseif ndims(ALL2) == 3
    out = mat2cell(A,f(dims(1,1),dims(2,1)),f(dims(1,2),dims(2,2)),f(dims(1,3),dims(2,3)));
end

end
