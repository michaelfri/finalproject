function img = normalizeD(iR,iI)
oR = ["R","G","W"];
for uA = 1:iR.sessions
    for uB = 1:iR.("R"+num2str(uA)).num
        [xR2{uA,uB},xR1{uA,uB}] = histcounts(iI.R{uA,uB}(:),144);
        [xG2{uA,uB},xG1{uA,uB}] = histcounts(iI.G{uA,uB}(:),144);
        [xW2{uA,uB},xW1{uA,uB}] = histcounts(iI.W{uA,uB}(:),144);
    end
end
T = {[45,83];[45,83];[45,83]};
for uC = 1:iR.sessions
    for uD = 1:iR.("R"+num2str(uC)).num
        for uE = 1:144
            if isempty(find(T{1}(:,1) == xR1{uC,uD}(uE)))
                T{1} = [T{1}; xR1{uC,uD}(uE) xR2{uC,uD}(uE)];
            else
                T{1}(find(T{1}(:,1) == xR1{uC,uD}(uE)),2) = T{1}(find(T{1}(:,1) == xR1{uC,uD}(uE)),2) + xR2{uC,uD}(uE);
            end
            if isempty(find(T{2}(:,1) == xG1{uC,uD}(uE)))
                T{2} = [T{2}; xG1{uC,uD}(uE) xG2{uC,uD}(uE)];
            else
                T{2}(find(T{2}(:,1) == xG1{uC,uD}(uE)),2) = T{2}(find(T{2}(:,1) == xG1{uC,uD}(uE)),2) + xG2{uC,uD}(uE);
            end
            if isempty(find(T{3}(:,1) == xW1{uC,uD}(uE)))
                T{3} = [T{3}; xW1{uC,uD}(uE) xW2{uC,uD}(uE)];
            else
                T{3}(find(T{3}(:,1) == xW1{uC,uD}(uE)),2) = T{3}(find(T{3}(:,1) == xW1{uC,uD}(uE)),2) + xW2{uC,uD}(uE);
            end
        end
    end
end
for uX = 1:3
    T{uX}(1,:) = [];
    T{uX} = sortrows(T{uX},1);
    M = max(T{uX}(49:96,2));
    d1 = max(find(T{uX}(1:48,2) >= M));
    d2 = min(find(T{uX}(97:end,2) >= M));
    raw = T{uX}(:,1);
    vals = [((0.05-1/length(raw)) *mat2gray(0.5*(raw(2:d1) + raw(1:(d1-1))))); (0.05+0.9*mat2gray(0.5*(raw((d1+1):d2) + raw(d1:(d2-1))))); (1-(0.05+1/length(raw))*mat2gray(-0.5*(raw((d2+1):end) + raw(d2:(end-1)))))];
    for uA = 1:iR.sessions
        for uB = 1:iR.("R"+num2str(uA)).num
            in = iI.("I"+num2str(uA,'%03d')+oR(uX)+num2str(uB,'%02d'));
            out = 0 * in;
            for uC = 1:length(vals)
                verts = find(in >= raw(uC) & in < raw(uC+1));
                out(verts) = vals(uC);
            end
            img.("I"+num2str(uA,'%03d')+oR(uX)+num2str(uB,'%02d')) = out;
        end
    end
end
