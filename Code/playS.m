function [ track ] = playS(dB,xspeech,xnoise,delay)
timeRef = datetime(1470144960+round(rand()*(posixtime(datetime('now'))-1470144960)), 'convertfrom','posixtime');
[noise,sr] = audioread(xnoise);
sr =44100;
speech = audioread(xspeech);
%if sr1 ~= sr
%    error('The audio file and the noise file have different sample rates');
%end
if min(size(speech)) == 1
    speech = [speech; speech];
end
if min(size(noise)) == 1
   noise = [noise noise noise; noise noise noise];
end
noise = [noise noise noise];
noise = [noise noise noise];
%marker = round(mod((time-timeRef)*sr-1,size(noise,1)/2)+1);
marker = round(mod(seconds(datetime-timeRef)*sr-1,size(noise,1)/2)+1);
d1=atan((0:(5/(delay(1)*sr)):5).^2-5);
d2=atan((5:(-5/(delay(2)*sr)):0).^2-5);
d_1=(d1-min(d1))./(2*max(d1)-2*min(d1));
d_1a=0:(0.5/(delay(1)*sr)):0.5;
d_2=(d2-min(d2))./(2*max(d2)-2*min(d2));
d_2a=0.5:(-0.5/(delay(2)*sr)):0;
trackS = [zeros(size(d_1,2),2); speech; zeros(size(d_2,2),2)];
disp(size([[d_1+d_1a; d_1+d_1a] ones(2,size(speech,1)) [d_2+d_2a; d_2+d_2a]]));
%disp(size(noise(marker:(marker+size(d1,2)+size(speech,1)+size(d2,2)-1),:)));
%trackN = [[d_1+d_1a; d_1+d_1a] ones(2,size(speech,1)) [d_2+d_2a; d_2+d_2a]]'.*noise(marker:(marker+size(d1,2)+size(speech,1)+size(d2,2)-1),:);
pt = randi(size(trackS,2));
trackN = noise((pt+1):(pt+size(trackS,1)));
disp(size(trackN));
disp(size(trackS));
%trackN = (([[d_1+d_1a; d_1+d_1a] ones(2,size(speech,1)) [d_2+d_2a; d_2+d_2a]]').^0).*noise(marker:(marker+size(d1,2)+size(speech,1)+size(d2,2)-1),:);
% track = (10^(dB/10))/((10^(dB/10))+1)*trackS+1/((10^(dB/10))+1)*trackN;
track = trackS'+trackN;
%ratio = [(10^(db/10))/((10^(db/10))+1) 1/((10^(db/10))+1)];
% make sure the two signals have the same loudness
%track(delay(1)*n(1):)
end


%soundsc((0.5*s+0.5*n(6435:(6434+size(s,1)),:)),44100)
