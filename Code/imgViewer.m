function figs = imgViewer(roi,plots,axis,prc)


for ss = 1:roi.sessions
    figs.Preview(ss) = figure('Name',"Preview, Session #"+num2str(ss),'Visible','off');imagesc(roi.("R"+num2str(ss)).preview);title("Preview, Session #"+num2str(ss));colorbar
    for num = 1:roi.("R"+num2str(ss)).num
        %THC
        figs.THC(ss,num) = figure('Name',"THC, Session #"+num2str(ss)+" Rep #" + num2str(num),'Visible','off');imagesc(prc.THC.("I"+num2str(ss,'%03d')+"R"+num2str(num,'%02d')));title("THC, Session #"+num2str(ss)+" Rep #" + num2str(num)+ " Mean=" + num2str(plots.THC(ss,num)));caxis(unifyAxis(axis.THC));colorbar
        %HBR
        figs.HBR(ss,num) = figure('Name',"HBR, Session #"+num2str(ss)+" Rep #" + num2str(num),'Visible','off');imagesc(prc.HBR.("I"+num2str(ss,'%03d')+"R"+num2str(num,'%02d')));title("HBR, Session #"+num2str(ss)+" Rep #" + num2str(num)+ " Mean=" + num2str(plots.HBR(ss,num)));caxis(unifyAxis(axis.HBR));colorbar
        %FLOW
        figs.FLOW(ss,num) = figure('Name',"Flow, Session #"+num2str(ss)+" Rep #" + num2str(num),'Visible','off');imagesc(prc.FLOW.("I"+num2str(ss,'%03d')+"R"+num2str(num,'%02d')));title("Flow, Session #"+num2str(ss)+" Rep #" + num2str(num)+ " Mean=" + num2str(plots.FLOW(ss,num)));caxis(unifyAxis(axis.FLOW));colorbar
        %SO2
        figs.SO2(ss,num) = figure('Name',"SO2, Session #"+num2str(ss)+" Rep #" + num2str(num),'Visible','off');imagesc(prc.SO2.("I"+num2str(ss,'%03d')+"R"+num2str(num,'%02d')));title("SO2, Session #"+num2str(ss)+" Rep #" + num2str(num)+ " Mean=" + num2str(plots.SO2(ss,num)));caxis(unifyAxis(axis.SO2));colorbar
        %Scattering
        figs.SCAT(ss,num) = figure('Name',"Scattering, Session #"+num2str(ss)+" Rep #" + num2str(num),'Visible','off');imagesc(prc.HBR(ss,num));title("Scattering, Session #"+num2str(ss)+" Rep #" + num2str(num)+ " Mean=" + num2str(plots.SCAT(ss,num)));caxis(unifyAxis(axis.SCAT));colorbar
        %
    end
end
