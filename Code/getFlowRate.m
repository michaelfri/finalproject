function [output] = getFlowRate(img,windowSize)
%
dim = size(img);
if mod(windowSize,2) == 0
    error('Error. Window Size can''t be an even number.')
else
    s = (windowSize - 1)/2;
end
sc_dev = zeros(dim(1),dim(2)); % Memory preallocation of the spatial correlation matrix.
sc_avg = zeros(dim(1),dim(2)); % Memory preallocation of the spatial correlation matrix.
for i = 1:dim(1)
    for j = 1:dim(2)
        window = img(max(1,i-s):min(dim(1),i+s),max(1,j-s):min(dim(2),j+s));
        sc_dev(i,j) = std2(window);
        sc_avg(i,j) = mean2(window);    
    end
end
    output=uint8(255.*sc_dev./sc_avg);
end


