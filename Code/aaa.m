function E = imgLoad(details)
deadPixel = [ 253 364 ];
E = struct;
E.etc = struct;
etc.sessions = sessions;
uX = 0;
N = 0;
TX{1} = {0};
idx(1,:) = [0,0,0,0];
for uA = 1:details.nOfSessions
    E.S(uA) = struct;
    [E.S(uA).name,E.S(uA).path] = uigetfile('*.mat');
    E.S(uA).num = 0;
    name = strsplit(E.S(uA).name,'-');
    E.S(uA).name = cell2mat(name(1));
    addpath(E.S(uA).path);
    
    while exist(E.S(uA).name + "-" + int2str(N + 1) + ".mat",'file') == 2
        N = N + 1;
    end
    E.S(uA).num = N;
    if mode == 'r'
        load(string(E.S(uA).path) + name(1) + "-" + name(2),'white');
        E.S(uA).preview = imPatch(white,deadPixel,'a');
        clear white;
        E.S(uA).preview = squeeze(E.S(uA).preview(3,:,:));
        figure('Name',char(['Preview - ' E.S(uA).name]),'units','normalized','outerposition',[0 0 1 1]);imagesc(E.S(uA).preview);title("White Light Illumination - Select ROI");colormap gray
        E.S(uA).roi = round(fliplr(ginput(1)));
        E.S(uA).preview = markROI(E.S(uA).preview,[E.S(uA).roi details.radius],10);
        close(char(['Preview - ' E.S(uA).name]));
        figure('Name',"ROI"+num2str(uA));imagesc(E.S(uA).preview);title("Selected ROI");colormap gray
        hold on;
        %rectangle('Position',[ROI.(char("R"+num2str(num))).roi(1),ROI.(char("R"+num2str(num))).roi(2),ROI.size*2,ROI.size*2],'LineWidth',2,'LineStyle','--')
        %hold off;
        
        load(E.S(uA).path + E.S(uA).name + "-" + int2str(uB) + ".mat",'white','red','green','blue');
        
        
        
        
        for uB = 1:N
            E.R{uA,uB} = imPatch(red,deadPixel,E.S(uA).roi,details.radius);
            E.G{uA,uB} = imPatch(green,deadPixel,E.S(uA).roi,details.radius);
            E.B{uA,uB} = imPatch(blue,deadPixel,E.S(uA).roi,details.radius);
            E.W{uA,uB} = imPatch(white,deadPixel,E.S(uA).roi,details.radius);
        end
    elseif mode == 'a'
        for uB = 1:N
            load(E.S(uA).path + E.S(uA).name + "-" + int2str(uB) + ".mat",'white','red','green','blue');
            for uB = 1:N
                E.R{uA,uB} = imPatch(red,deadPixel,'a');
                E.G{uA,uB} = imPatch(green,deadPixel,'a');
                E.B{uA,uB} = imPatch(blue,deadPixel,'a');
                E.W{uA,uB} = imPatch(white,deadPixel,'a');
            end
            clear green red white blue;
        end
        
    end
end