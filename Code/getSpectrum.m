function [spectrum] = getSpectrum(spect)
%GETSPECTRUM Get the wavelengths of the first spectrometer and save them in a double
% array.
global config;
spectrum(:,1) = invoke(spect, 'getWavelengths', config.spectConf.spectrometerIndex, config.spectConf.channelIndex);
spectrum(:,2) = invoke(spect, 'getSpectrum', config.spectConf.spectrometerIndex);
end