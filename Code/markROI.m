function out = markROI(in,roi,markerSize)
out = in;
out((roi(1)-1):(roi(1)+1),(roi(2)-markerSize+1):(roi(2)+markerSize-1)) = min(min(min(in),0));
out((roi(1)-markerSize+1):(roi(1)+markerSize-1),(roi(2)-1):(roi(2)+1)) = min(min(min(in),0));

out(roi(1),((roi(2)+1):(roi(2)+markerSize))) = max(max(max(in),1));
out(roi(1),((roi(2)-markerSize):(roi(2)-1))) = max(max(max(in),1));
out(((roi(1)+1):(roi(1)+markerSize)),roi(2)) = max(max(max(in),1));
out(((roi(1)-markerSize):(roi(1)-1)),roi(2)) = max(max(max(in),1));

sections = (round(-markerSize*1.5)):1:(2*roi(3));
sections = [sections(mod(sections,2*markerSize) == 0); sections(mod(sections,2*markerSize) == 0)+markerSize]';
sections(sections > (2*roi(3)+1)) = 2*roi(3)+1;
sections(sections < 1) = 1;
for i = 1:size(sections,1)
    out((roi(1)-roi(3)+sections(i,1)):(roi(1)-roi(3)+sections(i,2)),(roi(2)-roi(3))) = min(min(min(in),0));
    out((roi(1)-roi(3)+sections(i,1)):(roi(1)-roi(3)+sections(i,2)),(roi(2)+roi(3))) = min(min(min(in),0));
    out((roi(1)-roi(3)),(roi(2)-roi(3)+sections(i,1)):(roi(2)-roi(3)+sections(i,2))) = min(min(min(in),0));
    out((roi(1)+roi(3)),(roi(2)-roi(3)+sections(i,1)):(roi(2)-roi(3)+sections(i,2))) = min(min(min(in),0));

    out((roi(1)-roi(3)+sections(i,1)):(roi(1)-roi(3)+sections(i,2)),(roi(2)-roi(3)-1)) = max(max(max(in),1));
    out((roi(1)-roi(3)+sections(i,1)):(roi(1)-roi(3)+sections(i,2)),(roi(2)+roi(3)+1)) = max(max(max(in),1));
    out((roi(1)-roi(3)-1),(roi(2)-roi(3)+sections(i,1)):(roi(2)-roi(3)+sections(i,2))) = max(max(max(in),1));
    out((roi(1)+roi(3)+1),(roi(2)-roi(3)+sections(i,1)):(roi(2)-roi(3)+sections(i,2))) = max(max(max(in),1));
end