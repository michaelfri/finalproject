function mkVid(filename,pI,gP,aX,iI,iR,pR)
vid = VideoWriter(filename);
vid.FrameRate = 0.75;
vid.open
uC = 1;
scale = 377/(2*iR.size+1);
for uA = 1:iR.sessions
    if uA >= pR.PointOfDeath
        S{uA} = [pR.Colors(2,:), iR.(char("R"+num2str(uA))).num, 0.5*(pR.Colors(2,:)+1), 80, 79, 83, 84];
    else
        S{uA} = [pR.Colors(1,:), iR.(char("R"+num2str(uA))).num, 0.5*(pR.Colors(1,:)+1), 80, 82, 69];
    end
end
% G = figure('Name',"Graph",'Visible','off');
% for uA = 1:iR.sessions
%     for uB = 1:uA
%         for uC = ((uB-1)*S{uB}(4)):1:(uB*S{uB}(4))
%             figure(G);area(((uB-1)*S{uB}(4)):1:uC,[gP(max(uB-1,1),end) gP(uB,:)],'FaceColor',S{uB}(1:3),'EdgeColor',S{uB}(5:7),'LineWidth',1.5);xlim([0 numel(pI)]);
%             set(gca, 'units', 'normalized');
%             set(gca,'Position',[0,0,1,144/576]);
%             set(G,'innerPosition',[100,100,377,576]);
%             z1 = annotation(G,'textbox','Position',[144/377 521/576 233/377 55/576],'FontUnits','pixels','FitBoxToText','off','FontSize',50,'HorizontalAlignment','center','VerticalAlignment','middle','String',char(S{uA}(8:end)));
%             z2 = annotation(G,'textbox','Position',[0 521/576 144/377 55/576],'FontUnits','pixels','FitBoxToText','off','FontWeight','bold','FontSize',50,'HorizontalAlignment','center','VerticalAlignment','middle','BackgroundColor',S{uA}(1:3),'Color','w','EdgeColor',S{uA}(5:7),'LineWidth',1.5,'String',char(S{uA}(8:end)));
%             hold on
%         end
%     end
% 
%     
%        
%     hold on
% end
% 
% tmp = getframe(G);
% plt = tmp.cdata;
% clear tmp;
% 
% 
% G = figure(4);imshow(ones(233,576,3));
% set(gca, 'units', 'normalized','DataAspectRatio',[1 1 1],'Position',[0,0,1,1]);
% set(gcf,'innerPosition',[100,100,576,233]);
% 

for uA = 1:iR.sessions
    dead = (uA >= pR.PointOfDeath);
    for uB = 1:iR.(char("R"+num2str(uA))).num
        base(uC,:,:) = imresize(squeeze(iI.W{uA,uB}(3,:,:)),scale);
        cmap(uC,:,:) = imresize(pI{uA,uB},scale);
%         base(uC,:,:) = fliplr(squeeze(Is.(char("I"+num2str(uA,'%03d')+"W"+num2str(uB,'%02d')))(3,:,:)));
%         cmap(uC,:,:) = pI.(char("I"+num2str(uA,'%03d')+"R"+num2str(uB,'%02d')));
        POD(uC) = 1 + dead;
        SA{uC} = [gP(uA,uB) S{uA}];
        uC = uC + 1;
    end
end
disp([size(base) size(cmap)]);
gr = floor(48*mat2gray(reshape(gP',1,[])))+1;
raw = reshape(gP',1,[]);
format = [repmat('PRE \n%',(pR.PointOfDeath-1)*iR.R1.num,1); repmat('POST\n%',(iR.sessions - pR.PointOfDeath + 1)*iR.R1.num,1)];
format = [format num2str((floor(raw) + 1)') repmat('.',1,(uC-1))' num2str((floor(raw))') repmat('f',1,(uC-1))'];

visual = ones(50,round(1.3*uC),3);
%gr = gr(floor((1:1:200)*(length(gr)/201)+1));
%disp([size(gr) uA uB uC]);
for uD = 1:(uC-1)
    G{uD} = figure('Name',"Graph",'Visible','off');
    set(G{uD},'innerPosition',[100,100,377,432]);
    figure(G{uD});image(clrFusion(squeeze(base(uD,:,:)),squeeze(cmap(uD,:,:)),aX,'parula'));
    set(gca, 'units', 'normalized');
    set(gca,'Position',[0,0,1,377/432]);
    z1 = annotation(G{uD},'textbox','Position',[144/377 377/432 233/377 55/432],'FontUnits','pixels','FitBoxToText','off','FontSize',50,'HorizontalAlignment','center','VerticalAlignment','middle','String',num2str(SA{uD}(1),'% .2f'));
    z2 = annotation(G{uD},'textbox','Position',[0 377/432 144/377 55/432],'FontUnits','pixels','FitBoxToText','off','FontWeight','bold','FontSize',50,'HorizontalAlignment','center','VerticalAlignment','middle','BackgroundColor',SA{uD}(2:4),'Color','w','EdgeColor',SA{uD}(6:8),'LineWidth',1.5,'String',char(SA{uD}(9:end)));
end
for uE = 1:length(G)
        writeVideo(vid,getframe(G{uE}));
            close(G{uE});
end
vid.close;
end