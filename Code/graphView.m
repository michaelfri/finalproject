
function G = graphView(iR,gP,pR,ttl)
if length(ttl) == 1
    ttl(2) = "Arbitary Units";
end
    G = figure;area((1:1:iR.("R"+num2str(1)).num)',gP(1,:),'FaceColor',0.5*([1 1 1]+pR.Colors(1,:)),'EdgeColor',pR.Colors(1,:),'LineWidth',1.2);title(ttl(1));xlabel("Frame #");ylabel(ttl(2));
    inst = 1;
    hold on
    for uA = 2:iR.sessions
        for uB = 1:size(pR.timeLine,1)
            if ismember(uA,pR.timeLine{uB,1})
                inst(uA) = pR.timeLine{uB,2};
            end
        end
    end
    for uC = 2:iR.sessions
        area(((uC-1)*iR.("R"+num2str(uC)).num:1:uC*iR.("R"+num2str(uC)).num)',[gP(uC-1,end) gP(uC,:)],'FaceColor',0.5*([1 1 1]+pR.Colors(inst(uC),:)),'EdgeColor',pR.Colors(inst(uC),:),'LineWidth',1.2);
    end
    
    for uC = 1:length(pR.Events)
        switch inst(pR.Events(uC,1))
            case 1
                plot(((pR.Events(uC,1)-1)*iR.("R"+num2str(uA)).num+pR.Events(uC,2)),gP(pR.Events(uC,1),pR.Events(uC,2)),'g*', 'MarkerSize', 15);
            case 2
                plot(((pR.Events(uC,1)-1)*iR.("R"+num2str(uA)).num+pR.Events(uC,2)),gP(pR.Events(uC,1),pR.Events(uC,2)),'r*', 'MarkerSize', 15);
            otherwise
                plot(((pR.Events(uC,1)-1)*iR.("R"+num2str(uA)).num+pR.Events(uC,2)),gP(pR.Events(uC,1),pR.Events(uC,2)),'b*', 'MarkerSize', 15);
        end
        
    end
    set(G, 'Position', [120, 120, 1200, 400]);
    ax = gca;
    set(ax, 'InnerPosition', [0.05 0.12 0.93 0.82]);
    %range = [max(get(gca,'Xlim')) 0; 0 max(get(gca,'Ylim'))]*[0 1; 0 1] + [min(get(gca,'Xlim')) 0; 0 min(get(gca,'Ylim'))]*[1 -1; 1 -1];
    range = [max(get(gca,'Xlim')) 0; 0 max(get(gca,'Ylim'))]*[0 1; 0 1] + [min(get(gca,'Xlim')) 0; 0 min(get(gca,'Ylim'))]*[1 -1; 1 -1];
    scaling = get(gca,'Position');
    for uB = 2:size(pR.timeLine,1)
        %text((min(pR.timeLine{uB,1})-1)*iR.R1.num,gP(min(pR.timeLine{uB,1})-1,end),"\leftarrow " + pR.timeLine{uB,3},'FontSize',20);
        %p{uB} = [[ 1 1 ]*((max(pR.timeLine{uB,1})-1)*iR.R1.num - range(1,1))/range(1,2); [0.8 (gP(max(pR.timeLine{uB,1})-2,iR.R1.num) - range(2,1))/range(2,2)]];
        %disp((min(pR.timeLine{uB,1})-1)*iR.R1.num);
        %p{uB}(1,:) = p{uB}(1,:) * scaling(3) + scaling(1);
        %p{uB}(2,:) = p{uB}(2,:) * scaling(4) + scaling(2);
        %annotation('textarrow',p{uB}(1,:),p{uB}(2,:),'String',pR.timeLine{uB,3},'FontSize',18);
    end
    %annotation('textarrow',[(75-range(1,1))/range(1,2) (0.1-range(2,1))/range(2,2)],[(75-range(1,1))/range(1,2) (0.1-range(2,1))/range(2,2)],'String',"Death",'FontSize',18);
    hold off
end