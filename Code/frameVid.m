function frame = frameVid(filename,pI,aX,pR,iR,name)
% nums = f(abs(mean(reshape(frame,1,[]))),std(reshape(frame,1,[])));
% if mean(reshape(frame,1,[])) < 0
%     nums(1) = -nums(1);
% end
vid = VideoWriter(filename);
vid.FrameRate = 0.75;
vid.open
uC = 0;
nBars = 86;
nShow = @(x,y) [[x y]*10^(-max(floor(log10(abs(x))),floor(log10(abs(y))))) max(floor(log10(abs(x))),floor(log10(abs(y))))];
events = [(1:iR.R1.num)'.^0 (1:iR.R1.num)'];
        for uA = 2:iR.sessions
events = [events;  uA*ones(iR.("R"+num2str(uA)).num,1) (1:iR.(char("R"+num2str(uA))).num)'];
end
lims = [ inf -inf -inf ];
for uA = 1:size(events,1)
    iD = events(uA,:);
    data = reshape(pI{iD(1),iD(2)},1,[]);
    data(isnan(data)) = [];
    nums(uA,:) = nShow(mean(data),std(data));
    if nums(uA,3) == 0
        the_title{uA} = ("$" + num2str(nums(uA,1),'% .2f') + " \pm " + num2str(nums(uA,2),'% .4f') + "$");
    elseif nums(uA,3) == -1
        the_title{uA} = ("$" + num2str(0.1*nums(uA,1),'% .2f') + " \pm " + num2str(0.1*nums(uA,2),'% .4f') + "$");
    elseif nums(uA,3) == 1
        the_title{uA} = ("$" + num2str(10*nums(uA,1),'% .2f') + " \pm " + num2str(0.1*nums(uA,2),'% .4f') + "$");
    else
        the_title{uA} = ("$\left( " + num2str(nums(uA,1),'% .2f') + "  \pm " + num2str(nums(uA,2),'% .2f') + " \right) \times 10^{" + num2str(nums(uA,3)) + "}$");
    end
    dist = fitdist(data','normal');
    xN = icdf(dist,[0.0013499 0.99865]); % three-sigma range for normal distribution
    x{uA} = linspace(xN(1),xN(2));
    if ~dist.Support.iscontinuous
        % For discrete distribution use only integers
        x{uA} = round(x);
        x{uA}(diff(x{uA})==0) = [];
    end
    % Do histogram calculations
    [bCounts{uA},bEdges{uA}] = histcounts(data,nBars);
    bCtrs{uA} = bEdges{uA}(1:end-1)+diff(bEdges{uA})/2;
    lims = [min(lims(1),bCtrs{uA}(1)) max(lims(2),bCtrs{uA}(end)) max(lims(3),max(bCounts{uA}))];
    y{uA} = size(data,2) * (bEdges{uA}(2)-bEdges{uA}(1)) * pdf(dist,x{uA});
end
for uC = 1:size(events,1)
    iD = events(uC,:);
    out{uC} = figure('Visible','off');
    set(out{uC}, 'Position', [50, 50, 500,830],'color','w');
    if iD(1) < pR.PointOfDeath
        fig{uC}.hist = subplot(2,1,2);area(bCtrs{uC},bCounts{uC},'FaceColor',pR.Colors(1,:),'EdgeColor',0.5*([1 1 1]+pR.Colors(1,:)),'LineWidth',1.5);title(the_title{uC},'Interpreter','Latex','FontSize',20);xlim(lims(1:2));ylim([0 lims(3)]);
        fig{uC}.aX = gca;
        hold on
        plot(fig{uC}.aX,x{uC},y{uC},'b:','LineWidth',2);
        ttl = name + " Rep " + num2str(iD(1),'%02d') + ", Frame " + num2str(iD(2),'%02d') + " - Pre";
    else
        fig{uC}.hist = subplot(2,1,2);area(bCtrs{uC},bCounts{uC},'FaceColor',pR.Colors(2,:),'EdgeColor',0.5*([1 1 1]+pR.Colors(2,:)),'LineWidth',1.5);title(the_title{uC},'Interpreter','Latex','FontSize',20);xlim(lims(1:2));ylim([0 lims(3)]);
        fig{uC}.aX = gca;
        hold on
        plot(fig{uC}.aX,x{uC},y{uC},'b:','LineWidth',2);
        ttl = name + " Rep " + num2str(iD(1),'%02d') + ", Frame " + num2str(iD(2),'%02d') + " - Post";
    end
    %text(nums(1)-nums(2),1.04*fig{uA}.hist.YLim(2),num2str(nums(1))+""+num2str(nums(2)),'Color','black','FontSize',20,'FontWeight','bold');
    fig{uC}.image = subplot(2,1,1);imagesc(pI{iD(1),iD(2)});caxis(aX);colorbar;title(ttl,'FontSize',20);
    pbaspect([1 1 1]);
    fig{uC}.image.Position = [0.1,0.34,0.75,0.75];
    fig{uC}.hist.Position = [0.08,0.05,0.85,0.37];
end
for uD = 1:uC
        frame{uD} = getframe(out{uD}); drawnow
        writeVideo(vid, frame{uD});
end
close(vid);
end
