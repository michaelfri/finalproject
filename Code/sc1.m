function [sc] = sc1(img)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

roi = im2double(img);
std_filter = [ 0 0 0 0 1 0 0 0 0; 0 0 1 1 1 1 1 0 0; 0 1 1 1 1 1 1 1 0; 0 1 1 1 1 1 1 1 0; 1 1 1 1 1 1 1 1 1; 0 1 1 1 1 1 1 1 0; 0 1 1 1 1 1 1 1 0; 0 0 1 1 1 1 1 0 0; 0 0 0 0 1 0 0 0 0 ];
mean_filter = fspecial('disk',9); % Create averaging filter with dimensions (n x n) defined by user
sc = stdfilt(roi,std_filter)./imfilter(roi,mean_filter,'symmetric');
% Compute speckle contrast for each pixel in each image by dividing standard deviation of window by average pixel value in window
end