function M = meanD(data)
data = reshape(data,1,[]);
minmax = [min(data) max(data)];
data(data == minmax(1) | data == minmax(2)) = [];
M = mean(data(isfinite(data)),'all');
end