function [output] = practice(img)
source = double(img);
dim=size(source);
avg = mean2(source);
target = (min(255-avg,avg-1)/255)*(2*rand(dim(1),dim(2))-1); % Memory preallocation of the spatial correlation matrix.
output = uint8(avg+target.*source);
end


