function M = meanI(imgs)
O = zeros(2,size(imgs,2)*size(imgs,3));
img = reshape(imgs,size(imgs,1),size(imgs,2)*size(imgs,3));
img(img == min(min(img)) | img == max(max(img))) = NaN;
imgZ = img;
imgZ(isnan(imgZ)) = 0;
for i = 1:size(img,2)
    O(1,i) = sum(imgZ(:,i));
    O(2,i) = sum(isnumeric(img(:,i)));
end
M = O(1,:)./O(2,:);
M = reshape(M,size(imgs,2),size(imgs,3));
end
