function [conf] = configure()
% System Configuration
conf = struct();
conf.spectConf = struct();
conf.session = 0;
conf.pushBtn = 'A1';
conf.gain = [18 9];
conf.gainVect = linspace(0,conf.gain(1),conf.gain(2));
conf.srvPin = 'D11';
conf.ledPin = 'D12';
conf.pulseEnable = 'D13';
conf.trigger = 'D2';
conf.comPort = 'COM8';
conf.boardType = 'uno';
conf.srvPos = [0.25 0.32 0.1 0.97 0.25]; % Servo positions to allow each beam pass through.
conf.experimentPath = 'c:\Users\Student\Desktop\'
conf.spectConf.integrationTime = 50000; % integration time for sensor.
conf.spectConf.spectrometerIndex = 0; % Spectrometer index to use (first spectrometer by default).
conf.spectConf.channelIndex = 0; % Channel index to use (first channel by default).
conf.spectConf.enable = 1; % Enable flag.
%conf.pool = gcp;
end