function [sc] = sc2(img)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
img = im2double(img);
std_filter = [ 0 0 0 0 1 0 0 0 0; 0 0 1 1 1 1 1 0 0; 0 1 1 1 1 1 1 1 0; 0 1 1 1 1 1 1 1 0; 1 1 1 1 1 1 1 1 1; 0 1 1 1 1 1 1 1 0; 0 1 1 1 1 1 1 1 0; 0 0 1 1 1 1 1 0 0; 0 0 0 0 1 0 0 0 0 ];
mean_filter = fspecial('disk',9); % Create averaging filter with dimensions (n x n) defined by user
sc=0.*squeeze(img(1,:,:));
for i=2:min(size(img))
    sc = sc + (imfilter(squeeze(img(i,:,:)),mean_filter,'symmetric')./stdfilt(squeeze(img(i,:,:)),std_filter)).^2;
end
end