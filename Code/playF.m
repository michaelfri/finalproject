function [ track ] = playF(dB,lsts,xnoise,delay)
[noise,sr] = audioread(xnoise);
L = Fs/1000 * 20;       % 20 ms
S = L/2;                % 50% overlap (10 ms)
win = hanning (L, 'periodic');
delta = round(Fs/1000 * 5);    % 5 ms
Sout = S;
Sin = round(Sout/TSR);
pin=0; pout=0; nseg = 1; 
inlen = length(input_signal);
outlen = ceil(TSR*inlen+delta);
speech = zeros(outlen,1);
deltas = [];
speech(pout+1:pout+L) = input_signal(pin+1:pin+L);
pref = pin + Sout; 
pin = pin + Sin;
pout = pout + Sout;
while ( (pref + L) < inlen ) && ( (pin+L+delta) < inlen )
    reference_frame = input_signal(pref+1:pref+L);
    analysis_frame = input_signal(pin+1-delta:pin+L+delta); %.* win;
    [ xc, lags ] = xcorr(analysis_frame, reference_frame, 2*delta);
    aligned = 2*delta+1;
    xc_delta = xc(aligned:aligned+2*delta);
    [ ~, i ] = max(abs(xc_delta));
    xc_rms = rms(buffer(analysis_frame(1:L+2*delta),L,L-1,'nodelay'));
    [ ~, i ] = max(abs(xc_delta./xc_rms'));
    idx = i-1;
    deltas = [ deltas idx ];
    synthesis_frame = analysis_frame(idx+1:idx+L);
	speech(pout+1:pout+L) = ...
    speech(pout+1:pout+L) + synthesis_frame .* win;
    pref = pin - delta + idx + Sout;
    pin = pin + Sin;
    pout = pout + Sout;
    nseg = nseg + 1;
end
if min(size(speech)) == 1
    speech = [speech speech];
end
if min(size(noise)) == 1
    noise = [noise noise; noise noise];
end
disp(lsts.(char("List"+num2str(lst,'%02d'))).(char("Sen"+num2str(sen,'%02d'))).txt);
%marker = round(mod((time-timeRef)*sr-1,size(noise,1)/2)+1);
marker = round(mod(seconds(datetime-datetime(1450928773,'ConvertFrom','PosixTime','TimeZone','local'))*sr-1,size(noise,1)/2)+1);
d1=atan((0:(5/(delay(1)*sr)):5).^2-5);
d2=atan((5:(-5/(delay(2)*sr)):0).^2-5);
d_1=(d1-min(d1))./(2*max(d1)-2*min(d1));
d_1a=0:(0.5/(delay(1)*sr)):0.5;
d_2=(d2-min(d2))./(2*max(d2)-2*min(d2));
d_2a=0.5:(-0.5/(delay(2)*sr)):0;
trackS = [zeros(size(d_1,2),2); speech; zeros(size(d_2,2),2)];
trackN = [[d_1+d_1a; d_1+d_1a] ones(2,size(speech,1)) [d_2+d_2a; d_2+d_2a]]'.*noise(marker:(marker+size(d1,2)+size(speech,1)+size(d2,2)-1),:);
track = (10^(dB/10))/((10^(dB/10))+1)*trackS+1/((10^(dB/10))+1)*trackN;

%ratio = [(10^(db/10))/((10^(db/10))+1) 1/((10^(db/10))+1)];
% make sure the two signals have the same loudness
%track(delay(1)*n(1):)
end


%soundsc((0.5*s+0.5*n(6435:(6434+size(s,1)),:)),44100)