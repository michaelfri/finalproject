function figs = imgViewer(roi,plots,axis,prc)
unifyAxis(ax1,ax2)

for ss = 1:roi.sessions
    figs.Preview(ss) = figure('Name',"Preview, Session #"+num2string(ss),'Visible','off');imagesc(roi.("R"+num2str(ss)).preview);title("Preview, Session #"+num2string(ss));colorbar
    for num = 1:roi.("R"+num2str(ss)).num
        %THC
        figs.THC(ss,num) = figure('Name',"THC, Session #"+num2string(ss)+" Rep #" + num2string(num),'Visible','off');imagesc(prc.THC(ss,num));title("THC, Session #"+num2string(ss)+" Rep #" + num2string(num)+ " Mean=" + num2str(mean2(prc.THC(ss,num))));colorbar


end
