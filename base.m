function [vect] = base(x)
if x >= 15
    a=15*(x/15-floor(x/15));
    vect = [ a base((x-a)/15)];
else 
    vect = 15*(x/15-floor(x/15));
end