function [stdw,avgw] = mojo(img)
avgw = zeros(size(img,1),size(img,2),3);
stdw = zeros(size(img,1),size(img,2),3);
c = 0
for k=1:3
img1 = zeros(size(img,1)+10,size(img,2)+10);
img1 = [fliplr(img(:,1:5,k)) img(:,:,k) fliplr(img(:,(end-4):end,k))];
img1 = [flipud(img1(1:5,:)); img1; flipud(img1((end-4):end,:))];

window = zeros(i,j,49);
for ptX = 1:size(img,1)
for ptY= 1:size(img,2)
window(ptY,ptX,1) = img1(ptX+6,ptY+2);
window(ptY,ptX,49) = img1(ptX+6,ptY+10);
window(ptY,ptX,2:6) = img1((ptX+4):(ptX+8),ptY+3);
window(ptY,ptX,44:48) = img1((ptX+4):(ptX+8),ptY+9);
window(ptY,ptX,7:13) = img1((ptX+3):(ptX+9),ptY+4);
window(ptY,ptX,37:43) = img1((ptX+3):(ptX+9),ptY+8);
window(ptY,ptX,14:20) = img1((ptX+3):(ptX+9),ptY+5);
window(ptY,ptX,30:36) = img1((ptX+3):(ptX+9),ptY+7);
window(ptY,ptX,21:29) = img1((ptX+2):(ptX+10),ptY+6);
end
disp(floor((100/6)*c + (100/6)*(ptX/size(img,1))))
end
c=c+1;
s = permute(window,[3,1,2]);
for i = 1:size(img,1)
for j = 1:size(img,2)
stdw(i,j,k) = std(s(:,j,i));
avgw(i,j,k) = mean(s(:,j,i));
end
disp(floor((100/6)*c + (100/6)*(i/size(img,1))))
end
end
c=c+1;
end
