function [window] = window(img,pt)

ptX = pt(1);
ptY = pt(2);
if min(ptX,size(img,1)-ptX) < 4 || min(ptY,size(img,2)-ptY) < 4
    error('Point too close to edge')
end
window = zeros(1,49);
window(1,1) = img(ptX,ptY-4);
window(1,49) = img(ptX,ptY+4);
window(1,2:6) = img((ptX-2):(ptX+2),ptY-3);
window(1,44:48) = img((ptX-2):(ptX+2),ptY+3);
window(1,7:13) = img((ptX-3):(ptX+3),ptY-2);
window(1,37:43) = img((ptX-3):(ptX+3),ptY+2);
window(1,14:20) = img((ptX-3):(ptX+3),ptY-1);
window(1,30:36) = img((ptX-3):(ptX+3),ptY+1);
window(1,21:29) = img((ptX-4):(ptX+4),ptY);
end