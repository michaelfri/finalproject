function [out] = imgNorm(img,dist)
mid = double(img*(0.9/(dist(2) - dist(1))));
l = double(img-dist(1))*(0.9/(dist(2) - dist(1)));
h = double(img-dist(1))*(0.9/(dist(2) - dist(1)));
h(h<=0) = 0;
l(l>=0) = 0;
l = l - min(min(l));
h = h - min(min(h));
h = 0.05*(h/max(max(h))).^2;
if max(max(l)) > 0
    l = 0.05*(l/max(max(l))).^2;
end
out = mid;
out(out > 0.95) = 0.95;
out(out < 0.05) = 0;
out = out + l + h;
a = out;
a(a <= 1) = 0;
out = out - a; 
end
