function [ax,in] = mkcxs(in)
    img = reshape(in,size(in,1),[],1);
    t = ones(size(in,1)+1,1)*[-1 0 1]*(2^0.5+(2*pi())^(-2));
for i = 1:size(in,1)
    filtered = img(i,:);
    filtered(filtered == max(max(img)) | filtered == min(min(img))) = [];
    t(i,:) = t(i,:)*std2(filtered);
    t(i,:) = t(i,:) + mean(filtered);
end
general = reshape(img,1,[]);
general(general == max(max(general)) | general == min(min(general))) = [];
t(end,:) = t(end,:)*std(general);
t(end,:) = t(end,:) + mean(general);
ax = 0.75*[min(t(:,1)) max(t(:,3))] + 0.25*[t(end,1) t(end,3)];
end