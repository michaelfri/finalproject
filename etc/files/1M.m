function [patched] = imPatch(raw,pix,roi)
% A function meant for masking out dead pixels in the camera output.
% Specifically designed to work with the dimentions of matrices which are
% the output of the image acquisition function.
% The function recieves a channel to patch (i.e red, green, blue, white
% etc...) and a target pixel to mask out. The specific pixel is replaced
% with the average of 7x7 window around it in each one of the images in
% that channel. The pxiel is specified by [x y].
% Example use: patched_green = imPatch(green,[230 41]);
C = @(m) mean(m([5 12:16 20:26 29:35 37:40 42:45 47:53 56:62 66:70 77]));

R = cellfun(@(x) x(:,(pix(1)-4):(pix(1)+4),(pix(2)-4):(pix(2)+4)),raw,'UniformOutput',false);
for uA = 
for uA = 1:size(raw{1,1},1)
    A

num = size(raw);
if length(num) == 2
    h = num(1);
    w = num(2);
    num = 1;
elseif length(num) == 3
    h = num(2);
    w = num(3);
    num = num(1);
else
    error('Incompatible Dimensions');
end
patch = zeros(num,49);
patch(:,1) = raw(:,pix(1),pix(2)-4);
patch(:,2) = raw(:,pix(1)-4,pix(2));
patch(:,3:7) = raw(:,(pix(2)-2):(pix(2)+2),pix(1)-3);
patch(:,8:42) = reshape(raw(:,(pix(1)-3):(pix(1)+3),(pix(2)-2):(pix(2)+2)),num,[]);
patch(:,43:47) = raw(:,(pix(1)-2):(pix(1)+2),pix(2)+3);
patch(:,48) = raw(:,pix(1),pix(2)+4);
patch(:,49) = raw(:,pix(1)+4,pix(2));
patched = raw;
patched(:,pix(1),pix(2)) = mean(patch,2);
if roi ~= 'a'
    patched = patched(:,roi(1):(min((roi(1)+2*roi(3)),h)),roi(2):(min((roi(2)+2*roi(3)),w)));
end
end
