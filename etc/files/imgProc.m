function [prc,plots] = imgProc(roi,imgs)
plots = struct;
prc = struct;
c = size(imgs.("I"+num2str(1,'%03d')+"G"+num2str(1,'%02d')),1);
m_Hbr = [ 2000 10000 ];
m_HbO2 = [ 200 10000 ];
a = m_Hbr(1)/(m_Hbr(1)-m_HbO2(1));
b = m_Hbr(2)/(m_Hbr(1)-m_HbO2(1));
filter.std = [ 0 0 0 0 1 0 0 0 0; 0 0 1 1 1 1 1 0 0; 0 1 1 1 1 1 1 1 0; 0 1 1 1 1 1 1 1 0; 1 1 1 1 1 1 1 1 1; 0 1 1 1 1 1 1 1 0; 0 1 1 1 1 1 1 1 0; 0 0 1 1 1 1 1 0 0; 0 0 0 0 1 0 0 0 0 ];
filter.avg = fspecial('disk',9);

for ss = 1:roi.sessions
    for num = 1:double(roi("R"+num2str(ss)).num)
        %THC
        prc.THC.("I"+num2str(ss,'%03d')) = squeeze(mean(imgs.("I"+num2str(ss,'%03d')+"G"+num2str(num,'%02d')),1));
        plots.THC(ss,num) = mean(imgs.("I"+num2str(ss,'%03d')+"G"+num2str(num,'%02d')),'all');
        %HBR
        prc.HBR.("I"+num2str(ss,'%03d')) = squeeze(mean(imgs.("I"+num2str(ss,'%03d')+"R"+num2str(num,'%02d')),1));
        plots.HBR(ss,num) = mean(imgs.("I"+num2str(ss,'%03d')+"R"+num2str(num,'%02d')),'all');
        %FLOW
        for n = 1:c
            flow(n,:,:) = (imfilter(squeeze(imgs.("I"+num2str(ss,'%03d')+"R"+num2str(num,'%02d'))(n,:,:)),filter.avg,'symmetric')./stdfilt(squeeze(imgs.("I"+num2str(ss,'%03d')+"R"+num2str(num,'%02d'))(n,:,:)),filter.std)).^2;
        end
        prc.FLOW.("I"+num2str(ss,'%03d')) = squeeze(mean(flow,1));
        plots.FLOW(ss,num) = mean(flow,'all');
        clear flow;
        %SO2
        for n = 1:c
            so2(n,:,:) = a-b*(squeeze(imgs.("I"+num2str(ss,'%03d')+"R"+num2str(num,'%02d'))(n,:,:))./squeeze(imgs.("I"+num2str(ss,'%03d')+"G"+num2str(num,'%02d'))(n,:,:)));
        end
        prc.SO2.("I"+num2str(ss,'%03d')) = squeeze(mean(so2,1));
        plots.FLOW(ss,num) = mean(so2,'all');
        clear so2;
    end
end
end