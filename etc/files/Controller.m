﻿close all;
clear all;
clc;

roi = [ 0 0 0 0 ];
img = 4;
cAxis;
deadPixel = [ 0 0 ];
filter.std = [ 0 0 0 0 1 0 0 0 0; 0 0 1 1 1 1 1 0 0; 0 1 1 1 1 1 1 1 0; 0 1 1 1 1 1 1 1 0; 1 1 1 1 1 1 1 1 1; 0 1 1 1 1 1 1 1 0; 0 1 1 1 1 1 1 1 0; 0 0 1 1 1 1 1 0 0; 0 0 0 0 1 0 0 0 0 ];
filter.avg = fspecial('disk',9); 

[bl_name,bl_path] = uigetfile('*.mat');
load([bl_path bl_name]);
bl_R = imPatch(red,deadPixel);
bl_G = imPatch(green,deadPixel);
bl_W = imPatch(white,deadPixel);
clear green red white;
[p_name,p_path] = uigetfile('*.mat');
load([p_path p_name]);
p_R = imPatch(red,deadPixel);
p_G = imPatch(green,deadPixel);
p_W = imPatch(white,deadPixel);
clear green red white;

for n=1:size(bl_R,1)
    


roiFrame = zeros(size(bl_W,2),size(bl_W,2));
roiFrame(roi(1),roi(2):roi(4)) = 1;
roiFrame(roi(3),roi(2):roi(4)) = 1;
roiFrame(roi(1):roi(3),roi(2)) = 1;
roiFrame(roi(1):roi(3),roi(4)) = 1;

figure;image(squeeze(bl_W(img,:,:))+roiFrame);title('White Light Illumination');colormap gray

% White Image cut to ROI
figure;image(squeeze(bl_W(img,roi(1):roi(3),roi(2):roi(4))));title('White Light Illumination');colormap gray


%THC
figure;imagesc(squeeze(bl_G(img,roi(1):roi(3),roi(2):roi(4))));caxis(cAxis(1,:));title('THC, Baseline Green. Mean='+num2str(mean2(squeeze(bl_G(img,roi(1):roi(3),roi(2):roi(4))))));colorbar
figure;imagesc(squeeze(p_G(img,roi(1):roi(3),roi(2):roi(4))));caxis(cAxis(1,:));title('THC, Pressure Green. Mean='+num2str(mean2(squeeze(p_G(img,roi(1):roi(3),roi(2):roi(4))))));colorbar

%HBR
figure;imagesc(squeeze(bl_R(img,roi(1):roi(3),roi(2):roi(4))));caxis(cAxis(2,:));title('DeOxHb, Baseline Red. Mean='+num2str(mean2(squeeze(bl_R(img,roi(1):roi(3),roi(2):roi(4))))));colorbar
figure;imagesc(squeeze(p_R(img,roi(1):roi(3),roi(2):roi(4))));caxis(cAxis(2,:));title('DeOxHb, Pressure Red. Mean='+num2str(mean2(squeeze(p_R(img,roi(1):roi(3),roi(2):roi(4))))));colorbar

%FLOW
bl_flow = (imfilter(squeeze(bl_R(img,roi(1):roi(3),roi(2):roi(4))),filter.avg,'symmetric')./stdfilt(squeeze(bl_R(img,roi(1):roi(3),roi(2):roi(4))),filter.std)).^2;
p_flow = (imfilter(squeeze(p_R(img,roi(1):roi(3),roi(2):roi(4))),filter.avg,'symmetric')./stdfilt(squeeze(p_R(img,roi(1):roi(3),roi(2):roi(4))),filter.std)).^2;


figure;imagesc(bl_flow);title('Blood flow - Baseline. Mean='+num2str(mean2(bl_flow)));caxis(cAxis(3,:));colorbar
figure;imagesc(p_flow);title('Blood flow - Pressure. Mean='+num2str(mean2(p_flow)));caxis(cAxis(3,:));colorbar




%SO2
% G_BL_Norm=(green_baseline-min(min(green_baseline)))./(max(max(green_baseline))-min(min(green_baseline)));
% R_BL_Norm=(red_baseline-min(min(red_baseline)))./(max(max(red_baseline))-min(min(red_baseline)));
% G_P_Norm=(green_pressure-min(min(green_pressure)))./(max(max(green_pressure))-min(min(green_pressure)));
% R_P_Norm=(red_pressure-min(min(red_pressure)))./(max(max(red_pressure))-min(min(red_pressure)));
% G_BL_Norm(G_BL_Norm == 0) =10^-5;
% G_P_Norm(G_P_Norm == 0) =10^-5;
% R_BL_Norm(R_BL_Norm == 0) =10^-5;
% R_P_Norm(R_P_Norm == 0) =10^-5;
% tissue_saturation_baseline = (abs((G_BL_Norm - R_BL_Norm)./(G_BL_Norm))*100)/404;
% figure;imagesc(tissue_saturation_baseline);title('Tissue Saturation Baseline');colorbar;caxis([20 70])
% mean2(tissue_saturation_baseline)
% 
% 
% 
% tissue_saturation_baseline = ((rescale(green_baseline,10^-5,1)-rescale(red_baseline,10^-5,1)));
% figure;imagesc(tissue_saturation_baseline);title('Tissue Saturation Baseline');colorbar;%caxis([20 70])
% mean2(tissue_saturation_baseline)
% 
% 
% 
% tissue_saturation_baseline = (abs((green_baseline-red_baseline)./green_baseline))*193;
% figure;imagesc(tissue_saturation_baseline);title('Tissue Saturation baseline');colorbar;
% mean2(tissue_saturation_baseline)






%FUSIN WITH FLOW


% blood_flow_baseline_N=(blood_flow_baseline-min(min(blood_flow_baseline)))/(max(max(blood_flow_baseline))-min(min(blood_flow_baseline)));
% white_light_ROI_N=(white_light_ROI-min(min(white_light_ROI)))/(max(max(white_light_ROI))-min(min(white_light_ROI)));
% fusion_flow_baseline = blood_flow_baseline_N+white_light_ROI_N;
% figure;imagesc(fusion_flow_baseline);title('fusion flow baseline');colorbar;caxis([0.2 1.2])
% 
% blood_flow_pressure_N=(blood_flow_pressure-min(min(blood_flow_pressure)))/(max(max(blood_flow_pressure))-min(min(blood_flow_pressure)));
% white_light_ROI_N=(white_light_ROI-min(min(white_light_ROI)))/(max(max(white_light_ROI))-min(min(white_light_ROI)));
% fusion_flow_pressure = blood_flow_pressure_N+white_light_ROI_N;
% figure;imagesc(fusion_flow_pressure);title('fusion flow pressure');colorbar;caxis([0.2 1.2])


